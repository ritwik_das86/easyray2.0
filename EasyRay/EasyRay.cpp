// EasyRay.cpp : Defines the entry point for the console application.

/*****************************************************************************
EasyRay version 2.0

Copyright � 2020 Ritwik Das

This file is part of EasyRay 2.0 - Ray-tracing renderer for 3D scenes/models.

    EasyRay 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at 
	your option) any later version, subject to the following restrictions:
	
	1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software in 
	some published article, an acknowledgment or a link to the software 
	webpage would be appreciated.

	2. This notice may not be removed or altered from any source distribution.

    EasyRay 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.

	Author:
	Ritwik Das - ritwik.sami1990@gmail.com

****************************************************************************/

#include "stdafx.h"
#include "TracerOptix.h"
#include "TracerEmbree.h"
#include "Helper.hpp"
#include <GL/glut.h>
#include <tclap/CmdLine.h>
#include <tclap/Visitor.h>

// Mouse variables
bool mouseLeftDown;
bool mouseRightDown;
float mouseX, mouseY;

// Camera variables
float cameraAngleX;
float cameraAngleY;
float cameraDistance;
bool changed = true;

extern GLuint pixelBufferID;

// Global Tracer
TracerBase *rayTracer = NULL;

class CopyrightNotice : public TCLAP::Visitor
{
public:
        CopyrightNotice() : Visitor(){} ;
        void visit() 
		{ 
			std::string text("Copyright (c) 2020 Ritwik Das\n\n"

			"EasyRay 2.0 is free software: you can redistribute it and/or modify\n"
			"it under the terms of the GNU General Public License as published by\n"
			"the Free Software Foundation, either version 3 of the License, or (at\n"
			"your option) any later version, subject to the following restrictions:\n\n"
	
			"1. The origin of this software must not be misrepresented; you must not\n"
			"claim that you wrote the original software. If you use this software in\n"
			"some published article, an acknowledgment or a link to the software\n"
			"webpage would be appreciated.\n\n"

			"2. This notice may not be removed or altered from any source distribution.\n\n"

			"EasyRay 2.0 is distributed in the hope that it will be useful,\n"
			"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
			"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
			"GNU General Public License for more details.\n\n"

			"You should have received a copy of the GNU General Public License\n"
			"along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.\n\n"

			"Author:\n"
			"Ritwik Das - ritwik.sami1990@gmail.com\n\n");

			std::cout<<text;
		};
};

static void display()
{
	RTbuffer imageBuffer = 0;
	if (changed)
	{
		// Update camera
		const float angleX_rad = cameraAngleX * M_PIf / 180.0f;
		const float angleY_rad = cameraAngleY * M_PIf / 180.0f;
		optix::Matrix4x4 transform = optix::Matrix4x4::identity();
		transform *= optix::Matrix4x4::translate(make_float3(0, 0, cameraDistance));
		transform *= optix::Matrix4x4::rotate(angleX_rad, make_float3(1, 0, 0));
		transform *= optix::Matrix4x4::rotate(angleY_rad, make_float3(0, 1, 0));
		rayTracer->moveCamera(transform);

		// Render image
		imageBuffer = rayTracer->renderScene(BLINN_PHONG, false, false);
		changed = false;
	}
	RTresult result;
	GLvoid* imageData;
	GLsizei width, height;
	RTsize buffer_width, buffer_height;
	RTformat buffer_format;

	result = rtBufferMap(imageBuffer, &imageData);
	if (result != RT_SUCCESS) {
		// Get error from context
		RTcontext context;
		const char* error;
		rtBufferGetContext(imageBuffer, &context);
		rtContextGetErrorString(context, result, &error);
		fprintf(stderr, "Error mapping image buffer: %s\n", error);
		exit(2);
	}
	if (0 == imageData) {
		fprintf(stderr, "data in buffer is null.\n");
		exit(2);
	}

	result = rtBufferGetSize2D(imageBuffer, &buffer_width, &buffer_height);
	if (result != RT_SUCCESS) {
		// Get error from context
		RTcontext context;
		const char* error;
		rtBufferGetContext(imageBuffer, &context);
		rtContextGetErrorString(context, result, &error);
		fprintf(stderr, "Error getting dimensions of buffer: %s\n", error);
		exit(2);
	}
	width  = static_cast<GLsizei>(buffer_width);
	height = static_cast<GLsizei>(buffer_height);

	result = rtBufferGetFormat(imageBuffer, &buffer_format);
	GLenum gl_data_type = GL_FALSE;
	GLenum gl_format = GL_FALSE;
	switch (buffer_format) {
		case RT_FORMAT_UNSIGNED_BYTE4:
			gl_data_type = GL_UNSIGNED_BYTE;
			gl_format    = GL_RGBA;
			break;

		case RT_FORMAT_FLOAT:
			gl_data_type = GL_FLOAT;
			gl_format    = GL_LUMINANCE;
			break;

		case RT_FORMAT_FLOAT3:
			gl_data_type = GL_FLOAT;
			gl_format    = GL_RGB;
			break;

		case RT_FORMAT_FLOAT4:
			gl_data_type = GL_FLOAT;
			gl_format    = GL_RGBA;
			break;

		default:
			fprintf(stderr, "Unrecognized buffer data type or format.\n");
			exit(2);
			break;
	}

	glDrawPixels(width, height, gl_format, gl_data_type, imageData);
	glutSwapBuffers();

	// Now unmap the buffer
	result = rtBufferUnmap(imageBuffer);
	if (result != RT_SUCCESS) {
		// Get error from context
		RTcontext context;
		const char* error;
		rtBufferGetContext(imageBuffer, &context);
		rtContextGetErrorString(context, result, &error);
		fprintf(stderr, "Error unmapping image buffer: %s\n", error);
		exit(2);
	}
	rtBufferDestroy(imageBuffer);
}
static void keyPressed(unsigned char key, int x, int y)
{
  switch (key) {
  case 27: // esc
  case 'q':
	  if (rayTracer)
		  delete rayTracer;
    exit(EXIT_SUCCESS);
  }
}
void reshapeHandler(int w, int h)
{
	rayTracer->changeDimensions(w, h);
	changed = true;
}
void mouseMoveHandler(int x, int y)
{
	if (mouseLeftDown)
	{
		cameraAngleY += (x - mouseX);
		cameraAngleX += (y - mouseY);
		mouseX = x;
		mouseY = y;
	}
	if (mouseRightDown)
	{
		cameraDistance += (y - mouseY) * 0.2f;
		mouseY = y;
	}
}
void mouseClickHandler(int button, int state, int x, int y)
{
	mouseX = x;
	mouseY = y;

	if (button == GLUT_LEFT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			mouseLeftDown = true;
		}
		else if (state == GLUT_UP)
		{
			mouseLeftDown = false;
			changed = true;
			glutPostRedisplay();
		}
	}

	else if (button == GLUT_RIGHT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			mouseRightDown = true;
		}
		else if (state == GLUT_UP)
		{
			mouseRightDown = false;
			changed = true;
			glutPostRedisplay();
		}
	}
}
void initGlut(int* argc, char** argv)
{
	// Initialize GLUT
	glutInit(argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Final Render");
	glutKeyboardFunc(keyPressed);
	glutDisplayFunc(display);
	//glutReshapeFunc(reshapeHandler);
	glutMouseFunc(mouseClickHandler);
	glutMotionFunc(mouseMoveHandler);
}

int main(int argc, char* argv[])
{
	std::cout<<"Welcome to EasyRay 2.0 - by Ritwik Das!\n\n";
	std::cout<<"EasyRay 2.0 Copyright (c) 2020  Ritwik Das\n\n"<<
		"This program comes with ABSOLUTELY NO WARRANTY.\n"<<
		"This is free software, and you are welcome to redistribute it\n"<<
		"under certain conditions; use option '-c' for details.\n\n";

	// Parse command options
	try 
	{  
		TCLAP::CmdLine cmd("EasyRay - Ray-tracing renderer", ' ', "1.0");

		// Copyright notice
		TCLAP::SwitchArg copyArg("c", "copyright", "Copyright notice", cmd, false, new CopyrightNotice());

		// Renderer
		TCLAP::ValueArg<int> rtArg("r", "tracer", "Ray-tracing engine to use (0: Intel Embree (CPU), 1: Nvidia Optix (GPU), 2: Nvidia Optix Prime (GPU))", false, 1, "int", cmd);

		// Input file arg
		TCLAP::ValueArg<std::string> fileArg("s", "scene", "3D scene file (ending in .obj, .ply etc.)", false, "vase2/vase2.obj", "string", cmd);

		// Config file
		TCLAP::ValueArg<std::string> confArg("f", "config", "Configuration file (text file with details about light, camera etc. - see details about format in manual)", 
			false, "", "string", cmd);

		// Camera position
		TCLAP::ValueArg<std::string> camArg("m", "cam", "Position of the camera, e.g. [800,-400,0]", false, "[0,0,-600]", "[x,y,z]", cmd);
		TCLAP::ValueArg<std::string> lookatArg("t", "lookat", "Position of the object the camera is looking at, e.g. [0,0,0]", 
			false, "[0,0,0]", "[x,y,z]", cmd);

		// Lights
		TCLAP::MultiArg<std::string> lightArg("l", "light", "Lights to be included in scene rendering, e.g. [50,-30,0] [0,-40,0]", 
			false, "[x,y,z]", cmd);
		
		// Parse the argv array.
		cmd.parse(argc, argv);

		// Get the value parsed by each arg.
		std::string file = fileArg.getValue();
		const int engine = rtArg.getValue();
		const std::vector<std::string> lighting = lightArg.getValue();
		const std::string camPos = camArg.getValue();
		const std::string lookat = lookatArg.getValue();

		// Load the scene
		erScene scene;
		file = "glasses.obj";
		scene.LoadScene(file);
		scene.commit(true);

		// Add lights
		std::vector<erLight> lights;
		const size_t nLights = lighting.size();
		if (nLights == 0)
		{
			erLight l1; l1._lightPos = make_float4(0, 100, 300, 1);
			erLight l2; l2._lightPos = make_float4(0, 100, -300, 1);// l2.color = make_float3(1, 0, 0);
			lights.push_back(l1);
			lights.push_back(l2);
		}
		else
		{
			for(const std::string& light: lighting)
			{
				erLight l;
				std::stringstream ss;
				float3 temp;
				ss<<light;
				ss>>temp;
				l._lightPos = make_float4(temp, 1);
				lights.push_back(l);
			}
		}

		std::stringstream camStr, lookatStr;
		float3 temp;
		camStr<<camPos;
		camStr>>temp;
		const float4 camSource = make_float4(temp, 1);
		lookatStr<<lookat;
		lookatStr>>temp;
		const float4 camTarget = make_float4(temp, 1);

		// Create Ray-tracer
		switch (engine)
		{
		case 0:
			rayTracer = new TracerEmbree(scene, lights, camSource, camTarget);
			break;
		case 1:
			rayTracer = new TracerOptix(scene, lights, camSource, camTarget);
			break;
		case 2:
			rayTracer = new TracerOptixPrime(scene, lights, camSource, camTarget);
			break;
		}

		//static_cast<TracerOptixPrime*>(rayTracer)->renderLightPainting("vase.txt");
		//rayTracer->renderMultiLight("C:/Users/Ritwik/Dropbox/acmsiggraph/Paper/results/agp results for rendering/dragon2_guards_99.txt");

		// Init OpenGL
		initGlut(&argc, argv);
		std::cout<<"OpenGL version: "<<glGetString(GL_VERSION)<<std::endl<<std::endl;
		glutMainLoop();
	} 
	catch (TCLAP::ArgException &e)
	{ 
		std::cerr << "Error: " << e.error() << " for arg " << e.argId() << std::endl; 
	}

	return 0;
}

