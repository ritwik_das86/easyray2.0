/*****************************************************************************
EasyRay version 2.0

Copyright � 2020 Ritwik Das

This file is part of EasyRay 2.0 - Ray-tracing renderer for 3D scenes/models.

EasyRay 2.0 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software in
some published article, an acknowledgment or a link to the software
webpage would be appreciated.

2. This notice may not be removed or altered from any source distribution.

EasyRay 2.0 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.

Author:
Ritwik Das - ritwik.sami1990@gmail.com

****************************************************************************/

#include "stdafx.h"
#include "TracerOptixPrime.h"

TracerOptixPrime::TracerOptixPrime(const erScene& sc, const std::vector<erLight>& lightVec, float4 camSource, float4 camTarget)
	: TracerBase(camSource, camTarget),
	scene(sc),
	lightScene(NULL),
	nLights(static_cast<int>(lightVec.size())),
	lights(NULL)
{
	// Lights
	if (nLights > 0)
	{
		cux(cudaMallocManaged(&lights, nLights * sizeof(erLight), cudaMemAttachGlobal));
		cux(cudaMemcpy(lights, lightVec.data(), nLights * sizeof(erLight), cudaMemcpyDefault));
	}

	opx(rtpContextCreate(RTP_CONTEXT_TYPE_CUDA, &_context));
	unsigned int deviceNumbers[] = { 0, 1 };
	opx(rtpContextSetCudaDeviceNumbers(_context, 2, deviceNumbers));
	opx(rtpModelCreate(_context, &_model));

	// Create accel structures
	RTPbufferdesc vertsBD, trisBD;
	RTPbuffertype bType = RTP_BUFFER_TYPE_CUDA_LINEAR;
	opx(rtpBufferDescCreate(_context, RTP_BUFFER_FORMAT_VERTEX_FLOAT4, bType, scene._vertices, &vertsBD));
	opx(rtpBufferDescCreate(_context, RTP_BUFFER_FORMAT_INDICES_INT3, bType, scene._indices, &trisBD));
	opx(rtpBufferDescSetRange(vertsBD, 0, scene._nVertices));
	opx(rtpBufferDescSetRange(trisBD, 0, scene._nTriangles));
	opx(rtpModelSetTriangles(_model, trisBD, vertsBD));
	opx(rtpModelUpdate(_model, RTP_MODEL_HINT_ASYNC));
	opx(rtpModelFinish(_model));
	opx(rtpBufferDescDestroy(vertsBD));
	opx(rtpBufferDescDestroy(trisBD));
}

TracerOptixPrime::TracerOptixPrime(const erScene& scene)
	: TracerOptixPrime(scene, {}, {}, {})
{
}

TracerOptixPrime::~TracerOptixPrime()
{
	opx(rtpModelDestroy(_model));
	opx(rtpContextDestroy(_context));
}

// Wrapper for different shaders
cudaError_t TracerOptixPrime::RenderPixels(const erHit* hitInfo, float3* image, const Shader mode)
{
	switch (mode)
	{
	case PHONG:
	case BLINN_PHONG:
	case BECKMANN:
	case GAUSSIAN:
	case LAMBERTIAN:
		renderPixelsShader(hitInfo, mode, image);
		break;
	case LIGHT_PAINT:
		renderPixelsLightPaint(hitInfo, BLINN_PHONG, image);
		break;
	default:
		return cudaErrorInvalidValue;
	}
	return cudaDeviceSynchronize();
}

RTbuffer TracerOptixPrime::renderScene(const Shader mode, bool blur, bool saveFile, std::string savePath)
{
	AutoTimer _("Rendering (Optix Prime)...");
	RTbuffer image = 0;

	const unsigned int PIXELS = _dimensions.x * _dimensions.y;

	// Create first bounce rays
	erRay* rays;
	cux(cudaMalloc(&rays, PIXELS * sizeof(erRay)));
	cux(CreateCameraRays(rays, PIXELS, _dimensions, _cam));

	// Trace camera rays in the scene
	erHit* hits;
	cux(cudaMalloc(&hits, PIXELS * sizeof(erHit)));
	TraceRays(rays, PIXELS, hits);

	// Render pixels
	float3* DEVpixels;
	cux(cudaMalloc(&DEVpixels, PIXELS * sizeof(float3)));
	cux(RenderPixels(hits, DEVpixels, mode));

	// Copy buffer to host
	opx(rtBufferCreate(_rtc, RT_BUFFER_OUTPUT, &image));
	opx(rtBufferSetFormat(image, RT_FORMAT_FLOAT3));
	opx(rtBufferSetSize2D(image, _dimensions.x, _dimensions.y));
	void* data;
	opx(rtBufferMap(image, &data));
	cux(cudaMemcpy(data, DEVpixels, PIXELS * sizeof(float3), cudaMemcpyDefault));
	opx(rtBufferUnmap(image));

	cux(cudaFree(DEVpixels));
	cux(cudaFree(rays));
	cux(cudaFree(hits));

	if (blur)
		cux(ApplyGaussianBlur(image, _dimensions, 2, 0.79f));

	// Display or save image to file
	if (saveFile)
	{
		saveImageFile(savePath, image);
	}

	return image;
}

// Perform Optix Prime ray-tracing
void TracerOptixPrime::TraceRays(const erRay* rays, const unsigned int nRays, erHit* hits) const
{
	RTPbuffertype bType = RTP_BUFFER_TYPE_CUDA_LINEAR;
	RTPbufferdesc raysBD, hitsBD;
	opx(rtpBufferDescCreate(_context, erRay::format, bType, const_cast<erRay*>(rays), &raysBD));
	opx(rtpBufferDescCreate(_context, erHit::format, bType, hits, &hitsBD));
	opx(rtpBufferDescSetRange(raysBD, 0, nRays));
	opx(rtpBufferDescSetRange(hitsBD, 0, nRays));

	RTPquery query;
	opx(rtpQueryCreate(_model, RTP_QUERY_TYPE_CLOSEST, &query));
	opx(rtpQuerySetRays(query, raysBD));
	opx(rtpQuerySetHits(query, hitsBD));
	opx(rtpQueryExecute(query, RTP_QUERY_HINT_ASYNC));
	opx(rtpQueryFinish(query));
	opx(rtpQueryDestroy(query));

	opx(rtpBufferDescDestroy(raysBD));
	opx(rtpBufferDescDestroy(hitsBD));
}

void TracerOptixPrime::renderLightPainting(std::string lightTrajectoryFile)
{
	// Create sphere geometry for lights
	erScene lightscene;

	// Read the light trajectory
	int nPoints;
	std::ifstream fin(lightTrajectoryFile, std::ios::in);
	fin >> nPoints;
	lights = cudaReallocManaged<erLight>(lights, nLights, nPoints);
	nLights = nPoints;
	for (int i = 0; i < nPoints; i++)
	{
		erVertex vlight;
		fin >> vlight;
		lights[i]._lightPos = vlight;
		lights[i]._ambientCoefficient = 0.2f;
		lights[i]._attenuation = 0.0f;
		lights[i]._color = make_float3(1);
		lights[i]._turnedOn = false;

		// Add light bulbs
		optix::Matrix4x4 matrix = optix::Matrix4x4::identity();
		matrix *= optix::Matrix4x4::translate(make_float3(vlight));
		matrix *= optix::Matrix4x4::scale(make_float3(3));

		erMaterial mat;
		mat._diffColor = mat._emsvColor = mat._ambtColor = mat._specColor = mat._baseColor = make_float4(1, 0, 0, 1);
		mat._shininess = 10.0;

		lightscene.addInstance(
			erInstance(
				erGeometry("sphere.off"),	// for the light bulb
				mat.GetManagedCopy(),
				matrix));
	}
	fin.close();
	lightscene.commit(false);
	lightScene = &lightscene;

	// Render frames
	for (int i = -1; i < nPoints; i++)
	{
		//_cam.source = make_float4(cameraPoint(i * (2 * M_PIf) / 10), 1);
		std::cout << i << ": ";
		const int l = i;// (720 / nPoints);
		if (l >= 0 && l < nPoints)
			lights[l]._turnedOn = true;

		std::string savePath = "temp/scene";
		char light[4];
		_itoa_s(i, light, 4, 10);
		savePath += light;
		savePath += ".png";
		RTbuffer image = renderScene(LIGHT_PAINT, false, true, savePath);
		opx(rtBufferDestroy(image));
	}
}