/*****************************************************************************
EasyRay version 2.0

Copyright � 2020 Ritwik Das

This file is part of EasyRay 2.0 - Ray-tracing renderer for 3D scenes/models.

    EasyRay 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at 
	your option) any later version, subject to the following restrictions:
	
	1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software in 
	some published article, an acknowledgment or a link to the software 
	webpage would be appreciated.

	2. This notice may not be removed or altered from any source distribution.

    EasyRay 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.

	Author:
	Ritwik Das - ritwik.sami1990@gmail.com

****************************************************************************/

#ifndef HELPER_H
#define HELPER_H

#include <fstream>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <thrust/transform.h>
#include <optix_prime/optix_prime.h>
#include <optix_world.h>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <vector>
#include <chrono>

// Geometry parameters
#define CUBE_SIZE 500

//#define HEIGHT 1800
//#define WIDTH 3200 

//#define HEIGHT 1440
//#define WIDTH 2560

#define HEIGHT 1080
#define WIDTH 1920

//#define HEIGHT 540
//#define WIDTH 960

typedef float4 erVertex;
typedef int3 erTriangle;

// realloc() equivalent in device memory
template<typename T>
T* cudaReallocManaged(void* mem, size_t curSize, size_t newSize)
{
	void* newmem;
	if (newSize >= curSize && cudaMallocManaged(&newmem, newSize * sizeof(T), cudaMemAttachGlobal) == cudaSuccess)
	{
		cux(cudaMemcpy(newmem, mem, curSize * sizeof(T), cudaMemcpyDefault));
		cux(cudaFree(mem));
		return reinterpret_cast<T*>(newmem);
	}

	return NULL;
}

inline void cux(cudaError_t res)
{
	if (res != cudaSuccess)
	{
		std::cout << "\nCuda: ";
		std::cout << cudaGetErrorString(res) << std::endl;
	}
}

enum Shader
{
	PHONG,
	BLINN_PHONG,
	BECKMANN,
	GAUSSIAN,
	LAMBERTIAN,
	LIGHT_PAINT
};

struct erTransGeom 
{ 
	const float s;
	const float4 center;
	const optix::Matrix4x4 Rot;

	erTransGeom(float _s, float4 _center, optix::Matrix4x4 _Rot) : 
		s(_s),
		center(_center),
		Rot(_Rot)
	{} 
	
	__host__ __device__
	erVertex operator()(const erVertex& x) const 
	{ 
		return Rot * ((x - center) * s);
	} 
};

struct erRay 
{ 
	static const RTPbufferformat format = RTP_BUFFER_FORMAT_RAY_ORIGIN_TMIN_DIRECTION_TMAX;

	float3 origin;
	float tmin;
	float3 dir;
	float tmax;
};

struct erHit 
{ 
	static const RTPbufferformat format = RTP_BUFFER_FORMAT_HIT_T_TRIID_U_V;

	float _hit;
	int _id;
	float2 _uv;
};

struct erLight
{
	erVertex _lightPos		= make_float4(0.0f, 0.0f, -300.0f, 1.0f);
	float3 _color			= make_float3(0.6f);
	float _attenuation		= 0.0f;
	float _ambientCoefficient = 0.2f;
	bool _turnedOn			= true;
};


struct erCamera
{
	erVertex _source		= make_float4(0, 0, -300, 1);
	erVertex _target		= make_float4(0, 0, 0, 1);
	float3 _up				= make_float3(0, 1, 0);

	 // Field of view in angle (0 - 360)
	float _fovX				= 80;
	float _fovY				= 45;
};


struct erMaterial
{
	float4 _baseColor		= make_float4(0.5f);
	float4 _diffColor		= make_float4(0.5f);
	float4 _specColor		= make_float4(0.5f);
	float4 _ambtColor		= make_float4(0.01f);
	float4 _emsvColor		= make_float4(0.5f);
	float _shininess		= 10.0f;
	float _roughness		= 0.5f;
	float _refractionIndex	= 1.0f;
	float _opacity			= 1.0f;
	float _specStrength		= 1.0f;
	unsigned char* _texture	= NULL;
	unsigned int _texWidth	= 0;
	unsigned int _texHeight	= 0;

	erMaterial* GetManagedCopy();

	~erMaterial();
};


struct erGeometry
{
	std::vector<erVertex> _geomVertices;
	std::vector<erTriangle> _geomTriangles;
	std::vector<float3> _geomNormals;
	erVertex _BBox[2];

	erGeometry(std::vector<erVertex>&& verts, std::vector<erTriangle>&& triangs, std::vector<float3>&& norms, erVertex bbox[2]);
	erGeometry(std::string file);
};


struct erInstance
{
	erGeometry _geom;
	const erMaterial* _mat	= NULL;
	erVertex _BBox[2];

	erInstance(erGeometry&& geom, const erMaterial* mat, const optix::Matrix4x4 transform);
	erInstance(erInstance&& instance);
	~erInstance();
};


struct erScene 
{
	size_t _nVertices				= 0;
	size_t _nTriangles				= 0;
	std::vector<erInstance> _geomInstances;
	erInstance* _DEVgeomInstances	= NULL;
	erVertex _BBox[2];

	std::vector<size_t> _matID;
	size_t* _matID__D				= NULL;

	erVertex* _vertices				= NULL;
	std::vector<float3> _uv;
	float3* _uv__D					= NULL;
	float3* _normals__D				= NULL;
	erTriangle* _indices			= NULL;

	~erScene();

	void addInstance(erInstance&& inst);
	void calculateBBox();
	void commit(const bool transform);

	// Load a 3D model file into s (file = .obj, .ply, etc.)
	void LoadScene(std::string file);
};

using namespace std::chrono;

class AutoTimer
{
private:
	const time_point<high_resolution_clock> _start = high_resolution_clock::now();
	const std::string _msg;

public:
	AutoTimer(const std::string& msg)
		: _msg(msg)
	{
	}

	~AutoTimer()
	{
		const auto duration = high_resolution_clock::now() - _start;
		std::cout << _msg << " (" << duration_cast<milliseconds>(duration).count() << " ms)\n";
	}
};

// Applying Gaussian Blur on image
cudaError_t ApplyGaussianBlur(RTbuffer image, const int2 dim, const int kernelSize, const float sigma_squared);

// Save rendered image to disk
RTresult saveImageFile(std::string filename, RTbuffer buffer);

#endif