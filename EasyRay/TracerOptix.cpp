﻿/*****************************************************************************
EasyRay version 2.0

Copyright © 2020 Ritwik Das

This file is part of EasyRay 2.0 - Ray-tracing renderer for 3D scenes/models.

    EasyRay 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at 
	your option) any later version, subject to the following restrictions:
	
	1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software in 
	some published article, an acknowledgment or a link to the software 
	webpage would be appreciated.

	2. This notice may not be removed or altered from any source distribution.

    EasyRay 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.

	Author:
	Ritwik Das - ritwik.sami1990@gmail.com

****************************************************************************/

#include "stdafx.h"
#include "TracerOptix.h"

TracerOptix::TracerOptix(const erScene& sc, const std::vector<erLight>& lightVec, float4 camSource, float4 camTarget)
	: TracerOptixPrime(sc, lightVec, camSource, camTarget),
#ifdef _DEBUG
	engine("../EasyRay/x64/Debug/Engine.cu.ptx")
#else
	engine("../EasyRay/x64/Release/Engine.cu.ptx")
#endif
{
	AutoTimer _("Loading EasyRay...");

	// Global states
	RTvariable max_depth, radianceRay, shadowRay, epsilon, bg_color, bad_color, shader;
	opx(rtContextSetRayTypeCount(_rtc, 2));
	opx(rtContextSetEntryPointCount(_rtc, 1));
	opx(rtContextSetStackSize(_rtc, 4096));
	int devices[] = { 0, 1 };
	opx(rtContextSetDevices(_rtc, 2, devices));
	opx(rtContextDeclareVariable(_rtc, "max_depth", &max_depth));
	opx(rtContextDeclareVariable(_rtc, "radianceRay", &radianceRay));
	opx(rtContextDeclareVariable(_rtc, "shadowRay", &shadowRay));
	opx(rtContextDeclareVariable(_rtc, "epsilon", &epsilon));
	opx(rtContextDeclareVariable(_rtc, "bg_color", &bg_color));
	opx(rtContextDeclareVariable(_rtc, "bad_color", &bad_color));
	opx(rtContextDeclareVariable(_rtc, "shader", &shader));
	opx(rtVariableSet1i(max_depth, 24));
	opx(rtVariableSet1ui(radianceRay, 0));
	opx(rtVariableSet1ui(shadowRay, 1));
	opx(rtVariableSet1f(epsilon, 1.e-3f));
	opx(rtVariableSet3f(bg_color, 0.0f, 0.0f, 0.0f));
	opx(rtVariableSet3f(bad_color, 0.0f, 1.0f, 1.0f));

	// Camera info
	RTvariable cams, camt, camX, camY, camu, dims;
	opx(rtContextDeclareVariable(_rtc, "cam_source", &cams));
	opx(rtContextDeclareVariable(_rtc, "cam_target", &camt));
	opx(rtContextDeclareVariable(_rtc, "cam_fovX", &camX));
	opx(rtContextDeclareVariable(_rtc, "cam_fovY", &camY));
	opx(rtContextDeclareVariable(_rtc, "cam_up", &camu));
	opx(rtContextDeclareVariable(_rtc, "img_dim", &dims));
	opx(rtVariableSet3f(cams, _cam._source.x, _cam._source.y, _cam._source.z));
	opx(rtVariableSet3f(camt, _cam._target.x, _cam._target.y, _cam._target.z));
	opx(rtVariableSet1f(camX, _cam._fovX));
	opx(rtVariableSet1f(camY, _cam._fovY));
	opx(rtVariableSet3f(camu, _cam._up.x, _cam._up.y, _cam._up.z));
	opx(rtVariableSet2i(dims, _dimensions.x, _dimensions.y));

	// Context Programs
	RTprogram raygen, miss, exception;
	opx(rtProgramCreateFromPTXFile(_rtc, engine.c_str(), "RayGen_Pinhole", &raygen));
	opx(rtProgramCreateFromPTXFile(_rtc, engine.c_str(), "Miss_Radiance", &miss));
	opx(rtProgramCreateFromPTXFile(_rtc, engine.c_str(), "Exception", &exception));
	opx(rtContextSetRayGenerationProgram(_rtc, 0, raygen));
	opx(rtContextSetExceptionProgram(_rtc, 0, exception));
	opx(rtContextSetMissProgram(_rtc, 0, miss));

	finalize();
}

TracerOptix::~TracerOptix()
{
	cux(cudaFree(lights));
}

RTbuffer TracerOptix::renderScene(const Shader mode, bool blur, bool saveFile, std::string savePath)
{
	AutoTimer _("Rendering (Optix)...");
	RTbuffer output = 0;

	// Camera info
	RTvariable cams, camt, camX, camY, camu, dims, var, shader;
	opx(rtContextQueryVariable(_rtc, "cam_source", &cams));
	opx(rtContextQueryVariable(_rtc, "cam_target", &camt));
	opx(rtContextQueryVariable(_rtc, "cam_fovX", &camX));
	opx(rtContextQueryVariable(_rtc, "cam_fovY", &camY));
	opx(rtContextQueryVariable(_rtc, "cam_up", &camu));
	opx(rtContextQueryVariable(_rtc, "img_dim", &dims));
	opx(rtContextQueryVariable(_rtc, "shader", &shader));
	opx(rtVariableSet3f(cams, _cam._source.x, _cam._source.y, _cam._source.z));
	opx(rtVariableSet3f(camt, _cam._target.x, _cam._target.y, _cam._target.z));
	opx(rtVariableSet1f(camX, _cam._fovX));
	opx(rtVariableSet1f(camY, _cam._fovY));
	opx(rtVariableSet3f(camu, _cam._up.x, _cam._up.y, _cam._up.z));
	opx(rtVariableSet2i(dims, _dimensions.x, _dimensions.y));
	opx(rtVariableSet1i(shader, mode));

	// Create output buffer
	opx(rtBufferCreate(_rtc, RT_BUFFER_OUTPUT, &output));
	opx(rtBufferSetFormat(output, RT_FORMAT_UNSIGNED_BYTE4));
	opx(rtBufferSetSize2D(output, _dimensions.x, _dimensions.y));
	opx(rtContextQueryVariable(_rtc, "output_buffer", &var));
	opx(rtVariableSetObject(var, output));
	opx(rtContextValidate(_rtc));
	opx(rtContextLaunch2D(_rtc, 0, (RTsize)_dimensions.x, (RTsize)_dimensions.y));

	if (blur)
		cux(ApplyGaussianBlur(output, _dimensions, 2, 0.79f));

	// Display or save image to file
	if (saveFile)
	{
		saveImageFile(savePath, output);
	}

	return output;
}

/*void TracerOptix::renderFrames(std::string lightTrajectoryFile)
{
	// Read the light trajectory and render images
	int nPoints;
	std::ifstream fin(lightTrajectoryFile, std::ios::in);
	fin>>nPoints;
	for(int i = 0; i < nPoints; i++)
	{
		std::cout<<i<<": ";
		erVertex v;
		fin>>v;
		lights[0]._lightPos = v;

		// Render image and save it
		std::string savePath = "images/scene";
		char light[4];
		_itoa_s(i, light, 4, 10);
		savePath += light;
		savePath += ".png";
		RTbuffer image = renderSceneOptix(BLINN_PHONG, false, true, savePath);
		RTcontext context;
		opx(rtBufferGetContext(image, &context));
		opx(rtBufferDestroy(image));
		opx(rtContextDestroy(context));
	}
	fin.close();
}*/

float3 TracerOptix::cameraPoint(const float theta)
{
	const float3 C = make_float3(0);
	const float3 U = make_float3(1, 0, 0);
	const float3 V = make_float3(0, 0, 1);
	return C + 600 * cosf(theta) * U + 600 * sinf(theta) * V;
}

void TracerOptix::finalize()
{	
	AutoTimer _("Committing EasyRay scene...");

	RTprogram ch, ah, inter, bbox;
	void* data;
	// Material programs
	opx(rtProgramCreateFromPTXFile(_rtc, engine.c_str(), "ClosestHit_Radiance", &ch));
	opx(rtProgramCreateFromPTXFile(_rtc, engine.c_str(), "AnyHit_Shadow", &ah));
	// Geometry programs
	opx(rtProgramCreateFromPTXFile(_rtc, engine.c_str(), "Intersect", &inter));
	opx(rtProgramCreateFromPTXFile(_rtc, engine.c_str(), "Bounds", &bbox));
	
	RTgeometrygroup geomgrp;
	opx(rtGeometryGroupCreate(_rtc, &geomgrp));
	opx(rtGeometryGroupSetChildCount(geomgrp, static_cast<unsigned int>(scene._geomInstances.size())));
	int offset = 0;
	for(int inst = 0; inst < scene._geomInstances.size(); inst++)
	{
		auto&& geominst = scene._geomInstances[inst];
		// Geometry node
		RTgeometry geo;
		opx(rtGeometryCreate(_rtc, &geo));
		opx(rtGeometrySetPrimitiveCount(geo, static_cast<unsigned int>(geominst._geom._geomTriangles.size())));
		opx(rtGeometrySetPrimitiveIndexOffset(geo, offset));
		opx(rtGeometrySetBoundingBoxProgram(geo, bbox));
		opx(rtGeometrySetIntersectionProgram(geo, inter));
		offset += static_cast<int>(geominst._geom._geomTriangles.size());
		
		// Material node
		RTmaterial mate;
		opx(rtMaterialCreate(_rtc, &mate));
		opx(rtMaterialSetAnyHitProgram(mate, 1, ah));
		opx(rtMaterialSetClosestHitProgram(mate, 0, ch));
		const erMaterial& mat = *geominst._mat;
		RTvariable Ka, Kd, Ks, Sp, Ke, Sh, Rg, Re, Op, Diffuse_texture, valid_d_tex;
		opx(rtMaterialDeclareVariable(mate, "Ka", &Ka));
		opx(rtVariableSet3f(Ka, mat._ambtColor.x, mat._ambtColor.y, mat._ambtColor.z));
		opx(rtMaterialDeclareVariable(mate, "Kd", &Kd));
		opx(rtVariableSet3f(Kd, mat._diffColor.x, mat._diffColor.y, mat._diffColor.z));
		opx(rtMaterialDeclareVariable(mate, "Ks", &Ks));
		opx(rtVariableSet3f(Ks, mat._specColor.x, mat._specColor.y, mat._specColor.z));
		opx(rtMaterialDeclareVariable(mate, "Sp", &Sp));
		opx(rtVariableSet1f(Sp, mat._specStrength));
		opx(rtMaterialDeclareVariable(mate, "Ke", &Ke));
		opx(rtVariableSet3f(Ke, mat._emsvColor.x, mat._emsvColor.y, mat._emsvColor.z));
		opx(rtMaterialDeclareVariable(mate, "Sh", &Sh));
		opx(rtVariableSet1f(Sh, mat._shininess));
		opx(rtMaterialDeclareVariable(mate, "Rg", &Rg));
		opx(rtVariableSet1f(Rg, mat._roughness));
		opx(rtMaterialDeclareVariable(mate, "Re", &Re));
		opx(rtVariableSet1f(Re, mat._refractionIndex));
		opx(rtMaterialDeclareVariable(mate, "Op", &Op));
		opx(rtVariableSet1f(Op, mat._opacity));
		opx(rtMaterialDeclareVariable(mate, "valid_d_tex", &valid_d_tex));
			
		RTtexturesampler diffuse_tex;
		opx(rtTextureSamplerCreate(_rtc, &diffuse_tex));
		opx(rtTextureSamplerSetWrapMode(diffuse_tex, 0, RT_WRAP_REPEAT));
		opx(rtTextureSamplerSetWrapMode(diffuse_tex, 1, RT_WRAP_REPEAT));
		opx(rtTextureSamplerSetFilteringModes(diffuse_tex, RT_FILTER_LINEAR, RT_FILTER_LINEAR, RT_FILTER_NONE));
		opx(rtTextureSamplerSetIndexingMode(diffuse_tex, RT_TEXTURE_INDEX_NORMALIZED_COORDINATES));
		opx(rtTextureSamplerSetReadMode(diffuse_tex, RT_TEXTURE_READ_ELEMENT_TYPE));
		opx(rtTextureSamplerSetMaxAnisotropy(diffuse_tex, 1.0f));
		opx(rtTextureSamplerSetMipLevelCount(diffuse_tex, 1));
		opx(rtTextureSamplerSetArraySize(diffuse_tex, 1));
		opx(rtMaterialDeclareVariable(mate, "diffuse_map", &Diffuse_texture));

		RTbuffer diff_tex_buf;
		opx(rtBufferCreate(_rtc, RT_BUFFER_INPUT, &diff_tex_buf));
		opx(rtBufferSetFormat(diff_tex_buf, RT_FORMAT_UNSIGNED_BYTE4));
		opx(rtTextureSamplerSetBuffer(diffuse_tex, 0, 0, diff_tex_buf));
		opx(rtVariableSetObject(Diffuse_texture, diffuse_tex));
		if (mat._texture != NULL)
		{
			// Diffuse texture
			opx(rtVariableSet1i(valid_d_tex, 1));
			opx(rtBufferSetSize2D(diff_tex_buf, mat._texWidth, mat._texHeight));
			opx(rtBufferMap(diff_tex_buf, &data));
			cux(cudaMemcpy(data, mat._texture, mat._texWidth * mat._texHeight * sizeof(uchar4), cudaMemcpyDefault));
			opx(rtBufferUnmap(diff_tex_buf));
		}
		else
		{
			// Dummy buffer of size sizeof(uchar4)
			uchar4 dummy = make_uchar4(0, 0, 0, 0);
			opx(rtVariableSet1i(valid_d_tex, 0));
			opx(rtBufferSetSize2D(diff_tex_buf, 1, 1));
			opx(rtBufferMap(diff_tex_buf, &data));
			cux(cudaMemcpy(data, &dummy, sizeof(uchar4), cudaMemcpyDefault));
			opx(rtBufferUnmap(diff_tex_buf));
		}

		// Geometry Instance node
		RTgeometryinstance instance;
		opx(rtGeometryInstanceCreate(_rtc, &instance));
		opx(rtGeometryInstanceSetGeometry(instance, geo));
		opx(rtGeometryInstanceSetMaterialCount(instance, 1));
		opx(rtGeometryInstanceSetMaterial(instance, 0, mate));

		// Add to heirarchy
		opx(rtGeometryGroupSetChild(geomgrp, inst, instance));
	}

	// Acceleration node
	RTacceleration acc;
	opx(rtAccelerationCreate(_rtc, &acc));
	opx(rtAccelerationSetBuilder(acc, "Trbvh"));
	opx(rtAccelerationSetProperty(acc, "vertex_buffer_stride", "16"));           //sizeof(float4) = 16 bytes
	//rtAccelerationSetProperty(acc, "chunk_size", "-1");
	opx(rtGeometryGroupSetAcceleration(geomgrp, acc));
	RTvariable topobj, topshad;
	opx(rtContextDeclareVariable(_rtc, "top_object", &topobj));
	opx(rtContextDeclareVariable(_rtc, "top_shadower", &topshad));
	opx(rtVariableSetObject(topobj, geomgrp));
	opx(rtVariableSetObject(topshad, geomgrp));

	opx(rtGeometryGroupValidate(geomgrp));

	//RT buffers
	RTvariable var;
	RTbuffer ligh;
	opx(rtBufferCreate(_rtc, RT_BUFFER_INPUT, &ligh));
	opx(rtBufferSetFormat(ligh, RT_FORMAT_USER));
	opx(rtBufferSetElementSize(ligh, sizeof(erLight)));
	opx(rtBufferSetSize1D(ligh, nLights));
	opx(rtBufferMap(ligh, &data));
	cux(cudaMemcpy(data, lights, nLights * sizeof(erLight), cudaMemcpyDefault));
	opx(rtBufferUnmap(ligh));
	opx(rtContextDeclareVariable(_rtc, "lights", &var));
	opx(rtVariableSetObject(var, ligh));

	RTbuffer verts;
	opx(rtBufferCreate(_rtc, RT_BUFFER_INPUT, &verts));
	opx(rtBufferSetFormat(verts, RT_FORMAT_FLOAT4));
	opx(rtBufferSetSize1D(verts, scene._nVertices));
	opx(rtBufferMap(verts, &data));
	cux(cudaMemcpy(data, scene._vertices, scene._nVertices * sizeof(erVertex), cudaMemcpyDefault));
	opx(rtBufferUnmap(verts));
	opx(rtContextDeclareVariable(_rtc, "vertex_buffer", &var));
	opx(rtVariableSetObject(var, verts));

	RTbuffer norms;
	opx(rtBufferCreate(_rtc, RT_BUFFER_INPUT, &norms));
	opx(rtBufferSetFormat(norms, RT_FORMAT_FLOAT3));
	opx(rtBufferSetSize1D(norms, scene._nVertices));
	opx(rtBufferMap(norms, &data));
	cux(cudaMemcpy(data, scene._normals__D, scene._nVertices * sizeof(float3), cudaMemcpyDefault));
	opx(rtBufferUnmap(norms));
	opx(rtContextDeclareVariable(_rtc, "normal_buffer", &var));
	opx(rtVariableSetObject(var, norms));

	RTbuffer indi;
	opx(rtBufferCreate(_rtc, RT_BUFFER_INPUT, &indi));
	opx(rtBufferSetFormat(indi, RT_FORMAT_INT3));
	opx(rtBufferSetSize1D(indi, scene._nTriangles));
	opx(rtBufferMap(indi, &data));
	cux(cudaMemcpy(data, scene._indices, scene._nTriangles * sizeof(erTriangle), cudaMemcpyDefault));
	opx(rtBufferUnmap(indi));
	opx(rtContextDeclareVariable(_rtc, "index_buffer", &var));
	opx(rtVariableSetObject(var, indi));

	RTbuffer uvs;
	opx(rtBufferCreate(_rtc, RT_BUFFER_INPUT, &uvs));
	opx(rtBufferSetFormat(uvs, RT_FORMAT_FLOAT3));
	opx(rtBufferSetSize1D(uvs, scene._uv.size()));
	opx(rtBufferMap(uvs, &data));
	cux(cudaMemcpy(data, scene._uv__D, scene._uv.size() * sizeof(float3), cudaMemcpyDefault));
	opx(rtBufferUnmap(uvs));
	opx(rtContextDeclareVariable(_rtc, "uv", &var));
	opx(rtVariableSetObject(var, uvs));

	// Output buffer
	opx(rtContextDeclareVariable(_rtc, "output_buffer", &var));
}

void TracerOptix::renderMultiLight(std::string lightFile)
{
	// Create sphere geometry for lights
	erScene lightscene;

	// Read the light trajectory
	int nPoints;
	std::ifstream fin(lightFile, std::ios::in);
	fin>>nPoints;
	lights = cudaReallocManaged<erLight>(lights, nLights, nPoints);
	nLights = nPoints;
	for(int i = 0; i < nPoints; i++)
	{
		erVertex vlight;
		fin>>vlight;
		lights[i]._lightPos = vlight;
		lights[i]._ambientCoefficient = 0.2f;
		lights[i]._attenuation = 0.0f;
		lights[i]._color = make_float3(0.3f);
		float3 color;
		switch(i % 9)
		{
		case 0:
			color = make_float3(1, 0, 0);
			break;
		case 1:
			color = make_float3(0, 1, 0);
			break;
		case 2:
			color = make_float3(0, 0, 1);
			break;
		case 3:
			color = make_float3(1, 1, 0);
			break;
		case 4:
			color = make_float3(1, 0, 1);
			break;
		case 5:
			color = make_float3(0, 1, 1);
			break;
		case 6:
			color = make_float3(0.5, 0, 1);
			break;
		case 7:
			color = make_float3(1, 0.5, 0);
			break;
		case 8:
			color = make_float3(1, 0, 0.5);
			break;
		}
		lights[i]._color = make_float3(0.8f);//color;
		lights[i]._turnedOn = true;

		// Add light bulbs
		optix::Matrix4x4 matrix = optix::Matrix4x4::identity();
		matrix *= optix::Matrix4x4::translate(make_float3(vlight));
		matrix *= optix::Matrix4x4::scale(make_float3(5));
		
		erMaterial mat;
		mat._diffColor = mat._emsvColor = mat._ambtColor = mat._specColor = mat._baseColor = make_float4(1, 0, 0, 1);
		mat._shininess = 10.0;

		lightscene.addInstance(
			erInstance(
				erGeometry("sphere.off"),	// for the light bulb
				mat.GetManagedCopy(),
				matrix));
	}
	fin.close();
	lightscene.commit(false);
	lightScene = &lightscene;

	// Render frames
	for(int i = 0; i < 360; i++)
	{
		_cam._source = make_float4(cameraPoint(i * (2 * M_PIf) / 360), 1);
		std::cout<<i<<": ";
		if (i >= 0)
			lights[i]._turnedOn = true;

		std::string savePath = "images/scene";
		char light[4];
		_itoa_s(i, light, 4, 10);
		savePath += light;
		savePath += ".png";
		RTbuffer image = renderScene(LAMBERTIAN, false, true, savePath);
		RTcontext context;
		opx(rtBufferGetContext(image, &context));
		opx(rtBufferDestroy(image));
		opx(rtContextDestroy(context));
	}
}