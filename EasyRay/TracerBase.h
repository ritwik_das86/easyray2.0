/*****************************************************************************
EasyRay version 2.0

Copyright � 2020 Ritwik Das

This file is part of EasyRay 2.0 - Ray-tracing renderer for 3D scenes/models.

EasyRay 2.0 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software in
some published article, an acknowledgment or a link to the software
webpage would be appreciated.

2. This notice may not be removed or altered from any source distribution.

EasyRay 2.0 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.

Author:
Ritwik Das - ritwik.sami1990@gmail.com

****************************************************************************/
#pragma once

#include "Helper.hpp"

class TracerBase
{
protected:
	// Optix RT context
	RTcontext _rtc;

	// Render image dimensions
	int2 _dimensions = make_int2(WIDTH, HEIGHT);

	erCamera _cam;

	TracerBase(float4 camSource, float4 camTarget)
	{
		int rtx = 0;
		opx(rtGlobalSetAttribute(RT_GLOBAL_ATTRIBUTE_ENABLE_RTX, sizeof(int), &rtx));
		opx(rtContextCreate(&_rtc));

		// Camera
		_cam._source = camSource;
		_cam._target = camTarget;
	}

	void opx(RTresult res) const
	{
		if (res != RT_SUCCESS)
		{
			std::cout << "\nOptix: ";
			const char *str = new char[100];
			rtContextGetErrorString(_rtc, res, &str);
			std::cout << str << std::endl;
		}
	}

	static void opx(RTPresult res)
	{
		if (res != RTP_SUCCESS)
		{
			std::cout << "\nOptix Prime: ";
			const char *str = new char[100];
			rtpGetErrorString(res, &str);
			std::cout << str << std::endl;
		}
	}

public:
	virtual ~TracerBase()								{ opx(rtContextDestroy(_rtc)); }

	void changeDimensions(const int w, const int h)		{ _dimensions = make_int2(w, h);	}
	void moveCamera(optix::Matrix4x4 transform)			{ _cam._source = transform * _cam._source; }

	virtual RTbuffer renderScene(const Shader mode, bool blur, bool save = false, std::string savePath = "scene.png") abstract;
};