/*****************************************************************************
EasyRay version 2.0

Copyright � 2020 Ritwik Das

This file is part of EasyRay 2.0 - Ray-tracing renderer for 3D scenes/models.

EasyRay 2.0 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software in
some published article, an acknowledgment or a link to the software
webpage would be appreciated.

2. This notice may not be removed or altered from any source distribution.

EasyRay 2.0 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.

Author:
Ritwik Das - ritwik.sami1990@gmail.com

****************************************************************************/
#pragma once

#include "TracerBase.h"

class TracerOptixPrime : public TracerBase
{
private:
	RTPcontext _context;
	RTPmodel _model;

	// Perform Optix Prime ray-tracing
	void TraceRays(const erRay* rays, const unsigned int nRays, erHit* hits) const;

	// Wrapper for different shaders
	cudaError_t RenderPixels(const erHit* hitInfo, float3* image, const Shader mode);

	void renderLightScene(const erHit* hitInfo, float3* image);
	void renderPixelsShader(const erHit* hitInfo, const Shader mode, float3* image);
	void renderPixelsLightPaint(const erHit* hitInfo, const Shader mode, float3* image);

	// Camera rays
	static cudaError_t CreateCameraRays(erRay* rays, const unsigned int nRays, const int2 imageDim, const erCamera cam);

protected:
	// Scene to perform ray-tracing on
	const erScene& scene;

	// Scene containing light bulb geometries
	erScene* lightScene;

	// Array of lights
	erLight* lights;

	// Number of lights
	int nLights;

public:
	TracerOptixPrime(const erScene& scene, const std::vector<erLight>& lights, float4 camSource, float4 camTarget);
	TracerOptixPrime(const erScene& scene);
	~TracerOptixPrime();

	// Render scene using Optix Prime (no transparency, no reflection)
	RTbuffer renderScene(const Shader mode, bool blur, bool save = false, std::string savePath = "scene.png") override;

	void renderLightPainting(std::string lightTrajectoryFile);
};
