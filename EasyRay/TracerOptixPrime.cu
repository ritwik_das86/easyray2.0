/*****************************************************************************
EasyRay version 2.0

Copyright � 2020 Ritwik Das

This file is part of EasyRay 2.0 - Ray-tracing renderer for 3D scenes/models.

    EasyRay 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at 
	your option) any later version, subject to the following restrictions:
	
	1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software in 
	some published article, an acknowledgment or a link to the software 
	webpage would be appreciated.

	2. This notice may not be removed or altered from any source distribution.

    EasyRay 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.

	Author:
	Ritwik Das - ritwik.sami1990@gmail.com

****************************************************************************/

#include "TracerOptixPrime.h"

const unsigned int threadsperblock = 1024;

// Camera rays kernel
__global__ void computeCameraRay(erRay* rays, const int width, const int height, const erCamera cam)
{
	const size_t pix = blockDim.x * blockIdx.x + threadIdx.x;
	const float i = pix % width;
	const float j = pix / width;
	const float halfX = tanf((M_PIf / 180.0f) * cam._fovX / 2.0f);
	const float halfY = tanf((M_PIf / 180.0f) * cam._fovY / 2.0f);
	const float dx = 2.0f * halfX / width;
	const float dy = 2.0f * halfY / height;
	const float normalized_i = dx * (i - width / 2.0f);
	const float normalized_j = dy * (j - height / 2.0f);
	float3 camera_direction = optix::normalize(make_float3(cam._target - cam._source));
	float3 camera_right = -optix::normalize(optix::cross(camera_direction, cam._up));

	rays[pix].origin = make_float3(cam._source);
	rays[pix].dir = camera_direction + normalized_i * camera_right + normalized_j * cam._up;
	rays[pix].tmin = 0.0f;
	rays[pix].tmax = 1e34f;
}

// Initialize array to val
template<typename T>
__global__ void initialize(T* array, T val)
{
	const size_t i = blockDim.x * blockIdx.x + threadIdx.x;
	array[i] = val;
}

// Compute the background pixels and mask them
__global__ void maskBG(const erHit* hitInfo, float3* image)
{
	const size_t pix = blockDim.x * blockIdx.x + threadIdx.x;
	if (hitInfo[pix]._id < 0)       //background
		image[pix] = make_float3(-1);
	else
		image[pix] = make_float3(0.002);    //initialize to global ambient illumination
}

// Shadow rays kernel
__global__ void computeShadowRay(erRay* shadowRays, const erHit* hits, const erVertex* vertices, 
								 const erTriangle* indices, const erLight light)
{
	const size_t pix = blockDim.x * blockIdx.x + threadIdx.x;

	if (hits[pix]._id >= 0)
	{
		const erVertex v0 = vertices[indices[hits[pix]._id].x];
		const erVertex v1 = vertices[indices[hits[pix]._id].y];
		const erVertex v2 = vertices[indices[hits[pix]._id].z];
		const float3 hit = make_float3(
			v0 * hits[pix]._uv.x + 
			v1 * hits[pix]._uv.y + 
			v2 * (1 - hits[pix]._uv.x - hits[pix]._uv.y));

		shadowRays[pix].origin = hit;
		shadowRays[pix].dir = make_float3(light._lightPos) - hit;
		shadowRays[pix].tmin = 0.01f;
		shadowRays[pix].tmax = 1e34f;
	}
	else
	{
		shadowRays[pix].origin = make_float3(0.0f);
		shadowRays[pix].dir = make_float3(0.0f);
		shadowRays[pix].tmin = 0.0f;
		shadowRays[pix].tmax = 0.0f;
	}
}

// Merge scene and light scene (containing only light bulbs)
__global__ void zbufferMerge(const erHit* hitInfo, const erVertex* Gvertices, const erTriangle* Gindices, const erHit* lightHits, 
							 const erVertex* Lvertices, const erTriangle* Lindices, const erLight* lights, const erCamera cam, float3* image)
{
	const size_t pix = blockDim.x * blockIdx.x + threadIdx.x;
	const erHit lightHit = lightHits[pix];
	if (lightHit._id >= 0)
	{
		float3 lightColor = lights[lightHit._id / 960]._color;
		if (lights[lightHit._id / 960]._turnedOn)
		{
			const erHit geomHit = hitInfo[pix];
			if (geomHit._id >= 0)
			{
				const erVertex lightHitPos = 
					Lvertices[Lindices[lightHit._id].x] * lightHit._uv.x + 
					Lvertices[Lindices[lightHit._id].y] * lightHit._uv.y + 
					Lvertices[Lindices[lightHit._id].z] * (1 - lightHit._uv.x - lightHit._uv.y);
				const erVertex geomHitPos = 
					Gvertices[Gindices[geomHit._id].x] * geomHit._uv.x + 
					Gvertices[Gindices[geomHit._id].y] * geomHit._uv.y + 
					Gvertices[Gindices[geomHit._id].z] * (1 - geomHit._uv.x - geomHit._uv.y);
				const float distTolight = optix::length(lightHitPos - cam._source);
				const float distToGeom = optix::length(geomHitPos - cam._source);
				if (distTolight < distToGeom)
					image[pix] = lightColor;
			}
			else
				image[pix] = lightColor;
		}
	}
}

// Compute light contribution on each pixel and also cumulative contribution (total) of all lights
__global__ void computeLightContribution(const float3* image, const erVertex* vertices, const erTriangle* indices, const erHit* hitInfo, 
										 erHit* shadowHits, const erLight light, bool *mask, float* total = nullptr)
{
	const size_t pix = blockDim.x * blockIdx.x + threadIdx.x;
	mask[pix] = false;
	if (image[pix].x >= 0.0)
	{
		const erHit h0 = hitInfo[pix];
		const erVertex firstBounce = 
			vertices[indices[h0._id].x] * h0._uv.x + 
			vertices[indices[h0._id].y] * h0._uv.y + 
			vertices[indices[h0._id].z] * (1 - h0._uv.x - h0._uv.y);
		const float distTolight = optix::length(light._lightPos - firstBounce);
		float distToHit = 0.0f;
		if (shadowHits[pix]._id >= 0)
		{
			const erHit shadowHit = shadowHits[pix];
			const erVertex shadowOcclusion =
				vertices[indices[shadowHit._id].x] * shadowHit._uv.x + 
				vertices[indices[shadowHit._id].y] * shadowHit._uv.y + 
				vertices[indices[shadowHit._id].z] * (1 - shadowHit._uv.x - shadowHit._uv.y);
			distToHit = optix::length(shadowOcclusion - firstBounce);
		}

		if (shadowHits[pix]._id == -1 || distToHit > distTolight)
		{
			mask[pix] = true;

			if (total != nullptr)
				total[pix] += 1.0;
		}
	}
}

// Ambient layer
__global__ void applyAmbient(const erHit* hitInfo, erInstance* const instances, const size_t* matID, const bool* lightmask, const erLight* lights, 
							 int l, float3* image)
{
	const size_t pix = blockDim.x * blockIdx.x + threadIdx.x;
	if (lightmask[pix])
	{
		const erHit h = hitInfo[pix];
		const size_t triID = h._id;
		const erMaterial* mat = instances[matID[triID]]._mat;
		image[pix] += lights[l]._ambientCoefficient * make_float3(mat->_ambtColor) * lights[l]._color + make_float3(mat->_emsvColor);
	}
}

// Diffuse layer
__global__ void applyDiffuse(const erHit* hitInfo, erInstance* const instances, const size_t* matID, const bool* lightmask, const erLight* lights,
							 int l, const erVertex* vertices, const erTriangle* indices, const float3* uv, float3* image)
{
	const size_t pix = blockDim.x * blockIdx.x + threadIdx.x;
	if (lightmask[pix])
	{
		const erHit h = hitInfo[pix];
		const size_t triID = h._id;
		const erMaterial* mat = instances[matID[triID]]._mat;
		const erVertex v0 = vertices[indices[triID].x];
		const erVertex v1 = vertices[indices[triID].y];
		const erVertex v2 = vertices[indices[triID].z];
		const float3 e0 = make_float3(v1 - v0);
		const float3 e1 = make_float3(v2 - v1);
		const float3 normal = optix::normalize(optix::cross(e0, e1));
		const float3 surfacePos = make_float3(v0 * h._uv.x + v1 * h._uv.y + v2 * (1 - h._uv.x - h._uv.y));
		const float3 surfaceToLight = optix::normalize(make_float3(lights[l]._lightPos) - surfacePos);
		const float NdotS = optix::dot(normal, surfaceToLight);

		//attenuation
		const float distanceToLight = optix::length(make_float3(lights[l]._lightPos) - surfacePos);
		const float attenuation = 1.0 / (1.0 + lights[l]._attenuation * powf(distanceToLight / 100.0, 2));

		const float diffuseCoefficient = optix::fmaxf(0.0, NdotS);
		float3 diffColor = make_float3(mat->_diffColor);
		if (mat->_texture != NULL && uv)
		{
			// Calculate real texture coordinates
			const size_t W = mat->_texWidth;
			const size_t H = mat->_texHeight;
			float3 texPos = 
				uv[indices[triID].x] * h._uv.x + 
				uv[indices[triID].y] * h._uv.y + 
				uv[indices[triID].z] * (1 - h._uv.x - h._uv.y);
			texPos.x *= W;
			texPos.y *= H;
			while(texPos.x >= W)
			{
				texPos.x -= W;
			}
			while(texPos.x < 0)
			{
				texPos.x += W;
			}
			while(texPos.y >= H)
			{
				texPos.y -= H;
			}
			while(texPos.y < 0)
			{
				texPos.y += H;
			}
			const int2 texCoords = make_int2(texPos.x, texPos.y);
			const int offset = 4 * (texCoords.y * W + texCoords.x);
			diffColor = make_float3(
				mat->_texture[offset] / 255.0f, 
				mat->_texture[offset + 1] / 255.0f, 
				mat->_texture[offset + 2] / 255.0f);
		}
		image[pix] += diffuseCoefficient * diffColor * lights[l]._color * attenuation;
	}
}

// Specular layer (supports multiple shaders)
__global__ void applySpecular(const erHit* hitInfo, erInstance* const instances, const size_t* matID, const bool* lightmask, const erLight* lights,
							  int l, const erVertex* vertices, const erTriangle* indices, float3* image, const erCamera cam, const Shader mode)
{
	const size_t pix = blockDim.x * blockIdx.x + threadIdx.x;
	if (lightmask[pix])
	{
		const erHit h = hitInfo[pix];
		const size_t triID = h._id;
		const erMaterial* mat = instances[matID[triID]]._mat;
		const erVertex v0 = vertices[indices[triID].x];
		const erVertex v1 = vertices[indices[triID].y];
		const erVertex v2 = vertices[indices[triID].z];
		const float3 e0 = make_float3(v1 - v0);
		const float3 e1 = make_float3(v2 - v1);
		const float3 normal = optix::normalize(optix::cross(e0, e1));
		const float3 surfacePos = make_float3(v0 * h._uv.x + v1 * h._uv.y + v2 * (1 - h._uv.x - h._uv.y));
		const float3 surfaceToLight = optix::normalize(make_float3(lights[l]._lightPos) - surfacePos);
		const float3 surfaceToCamera = optix::normalize(make_float3(cam._source) - surfacePos);
		const float NdotH = optix::dot(normal, optix::normalize(surfaceToCamera + surfaceToLight));
		const float RdotV = optix::dot(surfaceToCamera, optix::reflect(-surfaceToLight, normal));
		const float NdotS = optix::dot(normal, surfaceToLight);

		//attenuation
		const float distanceToLight = optix::length(make_float3(lights[l]._lightPos) - surfacePos);
		const float attenuation = 1.0 / (1.0 + lights[l]._attenuation * powf(distanceToLight / 100.0, 2));

		float specularCoefficient = 0.0;
		const float diffuseCoefficient = optix::fmaxf(0.0, NdotS);
		if (diffuseCoefficient > 0.0)
		{
			switch(mode)
			{
			case PHONG:
				specularCoefficient = powf(optix::fmaxf(0.0, RdotV), mat->_shininess);
				break;
			case BLINN_PHONG:
				specularCoefficient = powf(optix::fmaxf(0.0, NdotH), mat->_shininess);
				break;
			case BECKMANN:
				specularCoefficient = powf(M_Ef, (powf(NdotH, 2.0f) - 1) / powf(NdotH * mat->_roughness, 2.0f)) / 
					(M_PIf * powf(mat->_roughness, 2.0f) * powf(NdotH, 4));
				break;
			case GAUSSIAN:
				specularCoefficient = powf(M_Ef, -powf(NdotH / mat->_roughness, 2.0f));
				break;
			}
		}
		const float3 specular = specularCoefficient * make_float3(mat->_specColor) * lights[l]._color;
		image[pix] += specular * mat->_specStrength * attenuation;
	}
}

// Add ambient, diffuse and specular layers for each pixel
__global__ void addChannels(float3* ambient, float3* diffuse, float3* specular, float3* image)
{
	const size_t pix = blockDim.x * blockIdx.x + threadIdx.x;
	if (image[pix].x < 0)
		image[pix] = make_float3(0);       //apply background color
	else
	{
		ambient[pix] = optix::clamp(ambient[pix], 0.0, 1.0);
		diffuse[pix] = optix::clamp(diffuse[pix], 0.0, 1.0);
		specular[pix] = optix::clamp(specular[pix], 0.0, 1.0);
		image[pix] += ambient[pix] + diffuse[pix] + specular[pix];
		image[pix] = optix::clamp(image[pix], 0.0, 1.0);
	}
}

// Add ambient, diffuse and specular layers normalized by the total contribution of all lights for each pixel 
__global__ void addChannels2(float3* ambient, float3* diffuse, float3* specular, const float* total, float3* image)
{
	const size_t pix = blockDim.x * blockIdx.x + threadIdx.x;
	if (image[pix].x < 0)
		image[pix] = make_float3(0);       //apply background color
	else
	{
		ambient[pix] = optix::clamp(ambient[pix] / optix::max(1.0f, total[pix]), 0.0, 1.0);
		diffuse[pix] = optix::clamp(diffuse[pix] / optix::max(1.0f, total[pix]), 0.0, 1.0);
		specular[pix] = optix::clamp(specular[pix] / optix::max(1.0f, total[pix]), 0.0, 1.0);
		image[pix] += ambient[pix] + diffuse[pix] + specular[pix];
		image[pix] = optix::clamp(image[pix], 0.0, 1.0);
	}
}

// Camera rays
cudaError_t TracerOptixPrime::CreateCameraRays(erRay* rays, const unsigned int nRays, const int2 imageDim, const erCamera cam)
{
	const unsigned int blocks = nRays / threadsperblock;
	computeCameraRay<<<blocks, threadsperblock>>>(rays, imageDim.x, imageDim.y, cam);
	return cudaDeviceSynchronize();
}

void TracerOptixPrime::renderLightScene(const erHit* hitInfo, float3* image)
{
	if (lightScene == nullptr)
		return;

	const unsigned int nPixels = _dimensions.x * _dimensions.y;
	const unsigned int blocks = nPixels / threadsperblock;

	// Create light rays
	erRay* rays;
	cux(cudaMalloc(&rays, nPixels * sizeof(erRay)));
	computeCameraRay<<<blocks, threadsperblock>>>(rays, _dimensions.x, _dimensions.y, _cam);
	cux(cudaDeviceSynchronize());

	TracerOptixPrime lightTracer(*lightScene);
	erHit* lightHits;
	cux(cudaMalloc(&lightHits, nPixels * sizeof(erHit)));
	lightTracer.TraceRays(rays, nPixels, lightHits);

	// Composite the geom scene and the light scene
	zbufferMerge<<<blocks, threadsperblock>>>(hitInfo, scene._vertices, scene._indices, lightHits, 
		lightScene->_vertices, lightScene->_indices, lights, _cam, image);
	cux(cudaDeviceSynchronize());
	cux(cudaFree(rays));
	cux(cudaFree(lightHits));
}

void TracerOptixPrime::renderPixelsShader(const erHit* hitInfo, const Shader mode, float3* image)
{
	const unsigned int nPixels = _dimensions.x * _dimensions.y;
	const unsigned int blocks = nPixels / threadsperblock;

	// Mask background pixels
	maskBG<<<blocks, threadsperblock>>>(hitInfo, image);

	// Create ambient, diffuse and specular channels
	float3 *ambient, *diffuse, *specular;
	cux(cudaMalloc(&ambient, nPixels * sizeof(float3)));
	cux(cudaMalloc(&diffuse, nPixels * sizeof(float3)));
	cux(cudaMalloc(&specular, nPixels * sizeof(float3)));

	initialize<<<blocks, threadsperblock>>>(ambient, make_float3(0));
	initialize<<<blocks, threadsperblock>>>(diffuse, make_float3(0));
	initialize<<<blocks, threadsperblock>>>(specular, make_float3(0));
	cux(cudaDeviceSynchronize());

	bool* lightMask;
	cux(cudaMalloc(&lightMask, nPixels * sizeof(bool)));

	erRay* shadowRays;
	cux(cudaMalloc(&shadowRays, nPixels * sizeof(erRay)));

	erHit* shadowHits;
	cux(cudaMalloc(&shadowHits, nPixels * sizeof(erHit)));

	//Now process contribution of each light
	for(int l = 0; l < nLights; l++)
	{
		// Check if light is turned ON
		if (!lights[l]._turnedOn)
			continue;

		// Create shadow rays for this light
		computeShadowRay<<<blocks, threadsperblock>>>(shadowRays, hitInfo, scene._vertices, scene._indices, lights[l]);
		cux(cudaDeviceSynchronize());

		TraceRays(shadowRays, nPixels, shadowHits);

		computeLightContribution<<<blocks, threadsperblock>>>(image, scene._vertices, scene._indices, hitInfo, shadowHits, lights[l], lightMask);

		// Apply ambient lighting
		applyAmbient<<<blocks, threadsperblock>>>(hitInfo, scene._DEVgeomInstances, scene._matID__D, lightMask, lights, l, ambient);

		// Apply diffuse lighting
		applyDiffuse<<<blocks, threadsperblock>>>(hitInfo, scene._DEVgeomInstances, scene._matID__D, lightMask, lights, l, 
			scene._vertices, scene._indices, scene._uv__D, diffuse);

		// Apply specular lghting
		if (mode != LAMBERTIAN)
		{
			applySpecular<<<blocks, threadsperblock>>>(hitInfo, scene._DEVgeomInstances, scene._matID__D, lightMask, lights, l, 
				scene._vertices, scene._indices, specular, _cam, mode);
		}

		cux(cudaDeviceSynchronize());
	}

	addChannels<<<blocks, threadsperblock>>>(ambient, diffuse, specular, image);

	// Free the memory
	cux(cudaFree(lightMask));
	cux(cudaFree(shadowRays));
	cux(cudaFree(shadowHits));
	cux(cudaFree(ambient));
	cux(cudaFree(diffuse));
	cux(cudaFree(specular));

	renderLightScene(hitInfo, image);
}

void TracerOptixPrime::renderPixelsLightPaint(const erHit* hitInfo, const Shader mode, float3* image)
{
	const unsigned int nPixels = _dimensions.x * _dimensions.y;
	const unsigned int blocks = nPixels / threadsperblock;

	// Mask background pixels
	maskBG<<<blocks, threadsperblock>>>(hitInfo, image);

	// Create ambient, diffuse and specular channels
	float3 *ambient, *diffuse, *specular;
	cux(cudaMalloc(&ambient, nPixels * sizeof(float3)));
	cux(cudaMalloc(&diffuse, nPixels * sizeof(float3)));
	cux(cudaMalloc(&specular, nPixels * sizeof(float3)));

	initialize<<<blocks, threadsperblock>>>(ambient, make_float3(0));
	initialize<<<blocks, threadsperblock>>>(diffuse, make_float3(0));
	initialize<<<blocks, threadsperblock>>>(specular, make_float3(0));

	// Total light constribution
	float* totalLight;
	cux(cudaMalloc(&totalLight, nPixels * sizeof(float)));
	initialize<<<blocks, threadsperblock>>>(totalLight, 0.0f);
	cux(cudaDeviceSynchronize());

	erRay* shadowRays;
	cux(cudaMalloc(&shadowRays, nPixels * sizeof(erRay)));

	bool* lightMask;
	cux(cudaMalloc(&lightMask, nPixels * sizeof(bool)));

	erHit* shadowHits;
	cux(cudaMalloc(&shadowHits, nPixels * sizeof(erHit)));

	//Now process contribution of each light
	for(int l = 0; l < nLights; l++)
	{
		// Check if light is turned ON
		if (!lights[l]._turnedOn)
			continue;

		// Create shadow rays for this light
		computeShadowRay<<<blocks, threadsperblock>>>(shadowRays, hitInfo, scene._vertices, scene._indices, lights[l]);
		cux(cudaDeviceSynchronize());

		TraceRays(shadowRays, nPixels, shadowHits);

		computeLightContribution<<<blocks, threadsperblock>>>(image, scene._vertices, scene._indices, hitInfo, shadowHits, 
			lights[l], lightMask, totalLight);

		// Apply ambient lighting
		applyAmbient<<<blocks, threadsperblock>>>(hitInfo, scene._DEVgeomInstances, scene._matID__D, lightMask, lights, l, ambient);

		// Apply diffuse lighting
		applyDiffuse<<<blocks, threadsperblock>>>(hitInfo, scene._DEVgeomInstances, scene._matID__D, lightMask, lights, l, 
			scene._vertices, scene._indices, scene._uv__D, diffuse);

		// Apply specular lghting
		applySpecular<<<blocks, threadsperblock>>>(hitInfo, scene._DEVgeomInstances, scene._matID__D, lightMask, lights, l, 
			scene._vertices, scene._indices, specular, _cam, mode);

		cux(cudaDeviceSynchronize());
	}

	addChannels2<<<blocks, threadsperblock>>>(ambient, diffuse, specular, totalLight, image);
	cux(cudaDeviceSynchronize());

	// Free the memory
	cux(cudaFree(lightMask));
	cux(cudaFree(shadowRays));
	cux(cudaFree(shadowHits));
	cux(cudaFree(ambient));
	cux(cudaFree(diffuse));
	cux(cudaFree(specular));
	cux(cudaFree(totalLight));

	renderLightScene(hitInfo, image);
}