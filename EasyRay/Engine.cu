/*****************************************************************************
EasyRay version 2.0

Copyright � 2020 Ritwik Das

This file is part of EasyRay 2.0 - Ray-tracing renderer for 3D scenes/models.

    EasyRay 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at 
	your option) any later version, subject to the following restrictions:
	
	1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software in 
	some published article, an acknowledgment or a link to the software 
	webpage would be appreciated.

	2. This notice may not be removed or altered from any source distribution.

    EasyRay 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.

	Author:
	Ritwik Das - ritwik.sami1990@gmail.com

****************************************************************************/

#include "Helper.hpp"
#include <optix.h>
#include <optix_world.h>

// Payload for ray type 0: visible rays
struct RadiancePL
{
	float3 color;
	int recursion_depth;
	float importance;
};

// Payload for ray type 1: shadow rays
struct ShadowPL
{
	float3 attenuation;
};

// Optix semantic variables
rtDeclareVariable(uint2, launch_index, rtLaunchIndex, "GPU thread index");
rtDeclareVariable(optix::Ray, cur_Ray, rtCurrentRay, "Ray in execution");
rtDeclareVariable(ShadowPL, shadow_PL, rtPayload, "Shadow Ray Payload");
rtDeclareVariable(RadiancePL, radiance_PL, rtPayload, "Radiance Ray Payload");
rtDeclareVariable(float, intersect_dist, rtIntersectionDistance, "Parametric intersection distance");

// Host variables
rtDeclareVariable(rtObject, top_object, , "Root node where Ray-Tracing context is launched");
rtDeclareVariable(rtObject, top_shadower, , "Root node where Ray-Tracing context is launched");
rtDeclareVariable(float, epsilon, , "Scene Epsilon");
rtDeclareVariable(int, shader, , "Shading mode");
rtDeclareVariable(int, max_depth, , "Max. recursion depth");
rtDeclareVariable(float3, bg_color, , "Background Color");
rtDeclareVariable(float3, bad_color, , "Error Color");
rtDeclareVariable(unsigned int, radianceRay, , "Radiance Ray type");
rtDeclareVariable(unsigned int, shadowRay, , "Shadow Ray type");

rtDeclareVariable(float3, cam_source, , "Camera source");
rtDeclareVariable(float3, cam_target, , "Camera target");
rtDeclareVariable(float, cam_fovX, , "Camera Horizontal Field of View");
rtDeclareVariable(float, cam_fovY, , "Camera Vertical Field of View");
rtDeclareVariable(float3, cam_up, , "Camera up");
rtDeclareVariable(int2, img_dim, , "Image Dimensions");

rtDeclareVariable(float3, Ka, , "Ambient");
rtDeclareVariable(float3, Kd, , "Diffuse");
rtDeclareVariable(float3, Ks, , "Specular");
rtDeclareVariable(float, Sp, , "Specular Strength");
rtDeclareVariable(float3, Ke, , "Emissive");
rtDeclareVariable(float, Sh, , "Shininess");
rtDeclareVariable(float, Rg, , "Roughness");
rtDeclareVariable(float, Re, , "Refraction Index");
rtDeclareVariable(float, Op, , "Opacity");

// Attributes
rtDeclareVariable(float3, geometric_normal, attribute geometric_normal, "Geometry Normal"); 
rtDeclareVariable(float3, shading_normal, attribute shading_normal, "Shading Normal"); 
rtDeclareVariable(float3, texcoord, attribute textureIdx, "Texture Coordinates");

// Textures
rtDeclareVariable(int, valid_d_tex, , "Valid diffuse texture");
rtTextureSampler<uchar4, 2> diffuse_map;

// Buffers
rtBuffer<uchar4, 2> output_buffer;
rtBuffer<erLight> lights;
rtBuffer<float4> vertex_buffer;
rtBuffer<int3> index_buffer;
rtBuffer<float3> normal_buffer;
rtBuffer<float3> uv;


RT_PROGRAM void RayGen_Pinhole()
{
	RadiancePL payload;
	payload.color = make_float3(0.0f);
	payload.importance = 1.0f;
	payload.recursion_depth = 0; // initialize recursion depth

	// Create ray
	const float width = img_dim.x;
	const float height = img_dim.y;
	const float i = launch_index.x;
	const float j = launch_index.y;
	const float halfX = tanf((M_PIf / 180.0f) * cam_fovX / 2.0f);
	const float halfY = tanf((M_PIf / 180.0f) * cam_fovY / 2.0f);
	const float dx = 2.0f * halfX / width;
	const float dy = 2.0f * halfY / height;
	const float normalized_i = dx * (i - width / 2.0f);
	const float normalized_j = dy * (j - height / 2.0f);
	const float3 camera_direction = optix::normalize(cam_target - cam_source);
	const float3 camera_right = -optix::normalize(optix::cross(camera_direction, cam_up));
	const float3 dir = optix::normalize(camera_direction + normalized_i * camera_right + normalized_j * cam_up);

	optix::Ray ray = optix::make_Ray(cam_source, dir, radianceRay, 0.0f, RT_DEFAULT_MAX);

	// Trace ray
	rtTrace(top_object, ray, payload);

	// Write result to output buffer
	const float3 color = optix::clamp(payload.color, 0.0f, 1.0f);
	output_buffer[launch_index] = make_uchar4(255 * payload.color.x, 255 * payload.color.y, 255 * payload.color.z, 0);
}

RT_PROGRAM void ClosestHit_Radiance()
{
	// Forward facing normals are used for most shading calculation except for refracted ray calculation
	const float3 ffnormal = optix::faceforward(shading_normal, -cur_Ray.direction, geometric_normal);
	const float3 surfacePos = cur_Ray.origin + intersect_dist * cur_Ray.direction;

	// Global ambient contribution
	float3 result = make_float3(0.02f);

	// Direct lighting
	float3 ambient = make_float3(0.0f);
	float3 diffuse = make_float3(0.0f);
	float3 specular = make_float3(0.0f);
	const int nLights = lights.size();
	for(int i = 0; i < nLights; i++) 
	{
		if (!lights[i]._turnedOn)
			continue;

		const float3 surfaceToLight = make_float3(lights[i]._lightPos) - surfacePos;
		const float NdotS = optix::dot(ffnormal, optix::normalize(surfaceToLight));

		// cast shadow ray
		float3 light_attenuation = make_float3(static_cast<float>(NdotS > 0.0f));
		if (NdotS > 0.0f) 
		{
			ShadowPL payload;
			payload.attenuation = make_float3(1.0f);
			optix::Ray shadow_ray = optix::make_Ray(surfacePos, optix::normalize(surfaceToLight), shadowRay, epsilon, optix::length(surfaceToLight));
			rtTrace(top_shadower, shadow_ray, payload);
			light_attenuation = payload.attenuation;
		}

		// If not completely shadowed, light the hit point
		if (optix::fmaxf(light_attenuation) > 0.0f) 
		{
			// Ambient
			ambient += Op * (lights[i]._ambientCoefficient * Ka * lights[i]._color + Ke);

			// Color after attenuation
			const float3 lightCol = lights[i]._color * light_attenuation;

			// Diffuse
			if (valid_d_tex == 1)
			{
				const uchar4 surfaceCol = tex2D(diffuse_map, texcoord.x, texcoord.y);
				diffuse += Op * (make_float3(surfaceCol.x, surfaceCol.y, surfaceCol.z) / 255.0f) * NdotS * lightCol;
			}
			else
				diffuse += Op * Kd * NdotS * lightCol;

			if (shader != LAMBERTIAN)
			{
				// Specular
				const float NdotH = optix::dot(ffnormal, optix::normalize(optix::normalize(surfaceToLight) - cur_Ray.direction));
				const float RdotV = optix::dot(-cur_Ray.direction, optix::reflect(-optix::normalize(surfaceToLight), ffnormal));
				if (NdotH > 0) 
				{
					float specularCoefficient = 0.0f;
					switch(shader)
					{
					case PHONG:
						specularCoefficient = powf(RdotV, Sh);
						break;
					case BLINN_PHONG:
						specularCoefficient = powf(NdotH, Sh);
						break;
					case BECKMANN:
						specularCoefficient = expf((powf(NdotH, 2.0f) - 1) / powf(NdotH * Rg, 2.0f)) / 
							(M_PIf * powf(Rg, 2.0f) * powf(NdotH, 4));
						break;
					case GAUSSIAN:
						specularCoefficient = expf(-powf(NdotH / Rg, 2.0f));
						break;
					}
					specular += Op * Ks * Sp * specularCoefficient * lightCol;
				}
			}
		}
	}

	result += optix::clamp(ambient, 0.0f, 1.0f);
	result += optix::clamp(diffuse, 0.0f, 1.0f);
	result += optix::clamp(specular, 0.0f, 1.0f);

	float reflection = 1.0f;

	// Refract
	const float3 Kt = make_float3(1.0f - Op);
	if (Op < 1.0f && radiance_PL.recursion_depth < max_depth) 
	{
		// ray tree attenuation
		RadiancePL new_payload;
		new_payload.recursion_depth = radiance_PL.recursion_depth + 1;

		// refraction ray		
		float3 R;
		if (optix::refract(R, cur_Ray.direction, shading_normal, Re)) 
		{
			float cos_theta = optix::dot(cur_Ray.direction, shading_normal);
			if (cos_theta < 0.0f)
				cos_theta = -cos_theta;
			else
				cos_theta = optix::dot(R, shading_normal);
			reflection = optix::fresnel_schlick(fabs(cos_theta));
			new_payload.importance = radiance_PL.importance * optix::luminance(Kt) * (1.0f - reflection);
			if (new_payload.importance >= 0.01f) 
			{
				optix::Ray refr_ray = optix::make_Ray(surfacePos, R, radianceRay, epsilon, RT_DEFAULT_MAX);
				rtTrace(top_object, refr_ray, new_payload);
				result += Kt * new_payload.color * (1.0f - reflection);
			} 
		}
	}

	// Reflect
	if (optix::fmaxf(Ks) > 0.0f && radiance_PL.recursion_depth < max_depth) 
	{
		// ray tree attenuation
		RadiancePL new_payload;
		new_payload.importance = radiance_PL.importance * optix::luminance(Ks) * reflection;
		new_payload.recursion_depth = radiance_PL.recursion_depth + 1;

		// reflection ray
		if (new_payload.importance >= 0.01f)
		{
			optix::Ray refl_ray = optix::make_Ray(surfacePos, optix::reflect(cur_Ray.direction, ffnormal), radianceRay, epsilon, RT_DEFAULT_MAX);
			rtTrace(top_object, refl_ray, new_payload);
			result += Ks * Sp * new_payload.color * reflection;
		}
	}

	// Attenuate incoming light and add the contribution to the current radiance ray�s payload 
	radiance_PL.color = optix::clamp(result, 0.0f, 1.0f);
}

RT_PROGRAM void Miss_Radiance()
{
  radiance_PL.color = bg_color;
}

RT_PROGRAM void AnyHit_Shadow()
{
	//const float3 world_shading_normal = optix::normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, shading_normal));

	const float fresnel = optix::fresnel_schlick(fabs(optix::dot(shading_normal, cur_Ray.direction)));

	const float3 Kt = make_float3(1.0f - Op);
	shadow_PL.attenuation *= (1.0f - fresnel);
  
	if (optix::luminance(shadow_PL.attenuation) < 0.01f)
		rtTerminateRay();
	else 
		rtIgnoreIntersection();
}

RT_PROGRAM void Intersect(int primIdx)
{
	const int3 v_ind = make_int3(index_buffer[primIdx].x, index_buffer[primIdx].y, index_buffer[primIdx].z);
	const float3 v1 = make_float3(vertex_buffer[v_ind.x]);
	const float3 v2 = make_float3(vertex_buffer[v_ind.y]);
	const float3 v3 = make_float3(vertex_buffer[v_ind.z]);

	float3 n;
	float t, u, v;
	if (optix::intersect_triangle(cur_Ray, v1, v2, v3, n, t, u, v))
	{ 
		if (rtPotentialIntersection(t))
		{
			geometric_normal = n;

			// Used for smooting the surfaces composed of primitives
			shading_normal = optix::normalize(normal_buffer[v_ind.x] * (1 - u - v) + normal_buffer[v_ind.y] * u + normal_buffer[v_ind.z] * v);
			texcoord = uv[v_ind.x] * (1 - u - v) + uv[v_ind.y] * u + uv[v_ind.z] * v;
			rtReportIntersection(0);
		}
	}
}

RT_PROGRAM void Bounds(int primIdx, float result[6])
{
	const erVertex v0 = vertex_buffer[index_buffer[primIdx].x];
	const erVertex v1 = vertex_buffer[index_buffer[primIdx].y];
	const erVertex v2 = vertex_buffer[index_buffer[primIdx].z];

	result[0] = optix::fminf(optix::fminf(v0.x, v1.x), v2.x);
	result[1] = optix::fminf(optix::fminf(v0.y, v1.y), v2.y);
	result[2] = optix::fminf(optix::fminf(v0.z, v1.z), v2.z);
	result[3] = optix::fmaxf(optix::fmaxf(v0.x, v1.x), v2.x);
	result[4] = optix::fmaxf(optix::fmaxf(v0.y, v1.y), v2.y);
	result[5] = optix::fmaxf(optix::fmaxf(v0.z, v1.z), v2.z);
}

RT_PROGRAM void Exception()
{
	rtPrintf("Caught exception 0x%X at launch index (%d,%d)\n", rtGetExceptionCode(), launch_index.x, launch_index.y);
	output_buffer[launch_index] = make_uchar4(255 * bad_color.x, 255 * bad_color.y, 255 * bad_color.z, 0);
}
