/*****************************************************************************
EasyRay version 2.0

Copyright � 2020 Ritwik Das

This file is part of EasyRay 2.0 - Ray-tracing renderer for 3D scenes/models.

EasyRay 2.0 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software in
some published article, an acknowledgment or a link to the software
webpage would be appreciated.

2. This notice may not be removed or altered from any source distribution.

EasyRay 2.0 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.

Author:
Ritwik Das - ritwik.sami1990@gmail.com

****************************************************************************/

#include "stdafx.h"
#include "TracerEmbree.h"
#include <rtcore_scene.h>
#include <tbb/parallel_for.h>

void TracerEmbree::AddVertices(const erScene& scene, RTCGeometry geom)
{
	float* vertices = (float*)rtcSetNewGeometryBuffer(geom,
		RTC_BUFFER_TYPE_VERTEX,
		0,
		RTC_FORMAT_FLOAT3,
		3 * sizeof(float),
		scene._nVertices);

	if (vertices != nullptr)
	{
		for (int i = 0; i < scene._nVertices; ++i)
		{
			vertices[3 * i] = scene._vertices[i].x;
			vertices[3 * i + 1] = scene._vertices[i].y;
			vertices[3 * i + 2] = scene._vertices[i].z;
		}
	}
}

void TracerEmbree::AddIndices(const erScene& scene, RTCGeometry geom)
{
	unsigned int* indices = (unsigned int*)rtcSetNewGeometryBuffer(geom,
		RTC_BUFFER_TYPE_INDEX,
		0,
		RTC_FORMAT_UINT3,
		3 * sizeof(unsigned int),
		scene._nTriangles);

	if (indices != nullptr)
	{
		for (int i = 0; i < scene._nTriangles; ++i)
		{
			indices[3 * i] = scene._indices[i].x;
			indices[3 * i + 1] = scene._indices[i].y;
			indices[3 * i + 2] = scene._indices[i].z;
		}
	}
}

TracerEmbree::TracerEmbree(const erScene& scene, const std::vector<erLight>& lights, float4 camSource, float4 camTarget)
	: TracerBase(camSource, camTarget)
	, _lights{lights}
{
	_device = rtcNewDevice(NULL);
	_scene = rtcNewScene(_device);

	RTCGeometry geom = rtcNewGeometry(_device, RTC_GEOMETRY_TYPE_TRIANGLE);
	AddIndices(scene, geom);
	AddVertices(scene, geom);

	/*
	* You must commit geometry objects when you are done setting them up,
	* or you will not get any intersections.
	*/
	rtcCommitGeometry(geom);

	/*
	* In rtcAttachGeometry(...), the scene takes ownership of the geom
	* by increasing its reference count. This means that we don't have
	* to hold on to the geom handle, and may release it. The geom object
	* will be released automatically when the scene is destroyed.
	*
	* rtcAttachGeometry() returns a geometry ID. We could use this to
	* identify intersected objects later on.
	*/
	rtcAttachGeometry(_scene, geom);
	rtcReleaseGeometry(geom);

	rtcCommitScene(_scene);
}

TracerEmbree::~TracerEmbree()
{
	rtcReleaseScene(_scene);
	rtcReleaseDevice(_device);
}

RTCRayHit TracerEmbree::CreatePrimaryRay(const int i, const int j)
{
	const float halfX = tanf((M_PIf / 180.0f) * _cam._fovX / 2.0f);
	const float halfY = tanf((M_PIf / 180.0f) * _cam._fovY / 2.0f);
	const float dx = 2.0f * halfX / _dimensions.x;
	const float dy = 2.0f * halfY / _dimensions.y;
	const float normalized_i = dx * (i - _dimensions.x / 2.0f);
	const float normalized_j = dy * (j - _dimensions.y / 2.0f);
	const float3 target{ _cam._target.x, _cam._target.y, _cam._target.z };
	const float3 source{ _cam._source.x, _cam._source.y, _cam._source.z };
	const float3 camera_direction = optix::normalize(target - source);
	const float3 camera_right = -optix::normalize(optix::cross(camera_direction, _cam._up));
	const float3 dir = optix::normalize(camera_direction + normalized_i * camera_right + normalized_j * _cam._up);

	RTCRayHit query;
	query.ray.dir_x = dir.x;
	query.ray.dir_y = dir.y;
	query.ray.dir_z = dir.z;

	query.ray.org_x = source.x;
	query.ray.org_y = source.y;
	query.ray.org_z = source.z;

	query.ray.tnear = 0.0f;
	query.ray.tfar = std::numeric_limits<float>::max();
	query.ray.time = 0.0f;

	query.hit.geomID = RTC_INVALID_GEOMETRY_ID;
	query.hit.primID = RTC_INVALID_GEOMETRY_ID;

	return query;
}

RTCRay TracerEmbree::CreateShadowRay(RTCRay primaryRay, const float3 lightDir)
{
	RTCRay shadow;
	shadow.org_x = primaryRay.org_x + primaryRay.tfar * primaryRay.dir_x;
	shadow.org_y = primaryRay.org_y + primaryRay.tfar * primaryRay.dir_y;
	shadow.org_z = primaryRay.org_z + primaryRay.tfar * primaryRay.dir_z;

	shadow.dir_x = -lightDir.x;
	shadow.dir_y = -lightDir.y;
	shadow.dir_z = -lightDir.z;

	shadow.tnear = 0.001f;
	shadow.tfar = std::numeric_limits<float>::max();
	shadow.time = 0.0f;

	return shadow;
}

float3 TracerEmbree::renderPixel(const int i, const int j)
{
	const float3 faceColor{ 0.3f, 0.3f, 0.3f };

	RTCIntersectContext context;
	rtcInitIntersectContext(&context);

	// Create ray
	RTCRayHit query = CreatePrimaryRay(i, j);

	rtcIntersect1(_scene, &context, &query);

	// shade pixels
	float3 color{};
	if (query.hit.geomID != RTC_INVALID_GEOMETRY_ID)
	{
		// initialize and trace shadow ray
		for (auto&& light : _lights)
		{
			if (!light._turnedOn)
				continue;

			const float3 rayOrigin{ query.ray.org_x, query.ray.org_y, query.ray.org_z };
			const float3 rayDir{ query.ray.dir_x, query.ray.dir_y, query.ray.dir_z };
			const float3 surfacePos = rayOrigin + query.ray.tfar * rayDir;
			const float3 lightPos{ light._lightPos.x, light._lightPos.y, light._lightPos.z };
			const float3 lightDir = optix::normalize(surfacePos - lightPos);
			const float3 surfaceColor = faceColor * light._color;

			// Ambient light
			color += optix::clamp(surfaceColor * light._ambientCoefficient, 0.0, 1.0);

			RTCRay shadow = CreateShadowRay(query.ray, lightDir);
			rtcOccluded1(_scene, &context, &shadow);

			// add light contribution
			if (shadow.tfar >= 0.0f)
			{
				// Diffuse light
				const float3 hitNormal{ query.hit.Ng_x, query.hit.Ng_y,query.hit.Ng_z };
				const float diffuseComp = optix::dot(-lightDir, optix::normalize(hitNormal));
				const float normaldiffComp = optix::clamp(diffuseComp, 0.0f, 1.0f);
				color += optix::clamp(surfaceColor * normaldiffComp, 0.0, 1.0);

				// Specular light
				const float3 ffnormal = optix::faceforward(hitNormal, -rayDir, hitNormal);
				const float RdotV = optix::dot(-rayDir, optix::reflect(lightDir, ffnormal));
				const float shininess = 0.1f;
				const float specularCoefficient = powf(RdotV, shininess);
				const float specStrength = 0.2f;
				color += optix::clamp(surfaceColor * specStrength * specularCoefficient, 0.0, 1.0);
			}
		}
	}

	return color;
}

RTbuffer TracerEmbree::renderScene(const Shader mode, bool blur, bool save, std::string savePath)
{
	AutoTimer _("Rendering (Embree)...");
	// Create output buffer
	RTbuffer output = 0;
	opx(rtBufferCreate(_rtc, RT_BUFFER_OUTPUT, &output));
	opx(rtBufferSetFormat(output, RT_FORMAT_FLOAT3));
	opx(rtBufferSetSize2D(output, _dimensions.x, _dimensions.y));

	void* data;
	opx(rtBufferMap(output, &data));

	tbb::parallel_for(tbb::blocked_range<int>(0, _dimensions.x), [=](tbb::blocked_range<int> r)
	{
		for (int i = r.begin(); i < r.end(); ++i)
		{
			for (int j = 0; j < _dimensions.y; ++j)
			{
				// Write result to output buffer
				reinterpret_cast<float3*>(data)[j * _dimensions.x + i] = renderPixel(i, j);
			}
		}
	});

	opx(rtBufferUnmap(output));
	return output;
}