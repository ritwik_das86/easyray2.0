/*****************************************************************************
EasyRay version 2.0

Copyright � 2020 Ritwik Das

This file is part of EasyRay 2.0 - Ray-tracing renderer for 3D scenes/models.

EasyRay 2.0 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software in
some published article, an acknowledgment or a link to the software
webpage would be appreciated.

2. This notice may not be removed or altered from any source distribution.

EasyRay 2.0 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.

Author:
Ritwik Das - ritwik.sami1990@gmail.com

****************************************************************************/
#pragma once

#include <rtcore_device.h>
#include <rtcore_geometry.h>
#include <rtcore_ray.h>
#include "TracerBase.h"

class TracerEmbree : public TracerBase
{
private:
	RTCDevice _device;
	RTCScene _scene;
	std::vector<erLight> _lights;

	static void AddVertices(const erScene& scene, RTCGeometry geom);
	static void AddIndices(const erScene& scene, RTCGeometry geom);
	static RTCRay CreateShadowRay(RTCRay primaryRay, const float3 lightDir);
	RTCRayHit CreatePrimaryRay(int i, int j);
	float3 renderPixel(const int i, const int j);

public:
	TracerEmbree(const erScene& scene, const std::vector<erLight>& lights, float4 camSource, float4 camTarget);
	~TracerEmbree();

	RTbuffer renderScene(const Shader mode, bool blur, bool save = false, std::string savePath = "scene.png") final;
};