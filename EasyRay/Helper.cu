/*****************************************************************************
EasyRay version 2.0

Copyright � 2020 Ritwik Das

This file is part of EasyRay 2.0 - Ray-tracing renderer for 3D scenes/models.

    EasyRay 2.0 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at 
	your option) any later version, subject to the following restrictions:
	
	1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software in 
	some published article, an acknowledgment or a link to the software 
	webpage would be appreciated.

	2. This notice may not be removed or altered from any source distribution.

    EasyRay 2.0 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.

	Author:
	Ritwik Das - ritwik.sami1990@gmail.com

****************************************************************************/

#include "Helper.hpp"
#include <Pathcch.h>
#include <device_launch_parameters.h>

const unsigned int threadsperblock = 1024;

__global__ void gaussianBlur(const float3* image, const int kernelSize, const float* gaussianMat, float3* output, 
							 const int2 imageDim)
{
	const size_t pix = blockDim.x * blockIdx.x + threadIdx.x;
	const int i = pix / imageDim.x;
	const int j = pix % imageDim.x;
	const int mat_dim = 2 * kernelSize + 1;
	const int offset_i = i - kernelSize;
	const int offset_j = j - kernelSize;
	output[pix] = make_float3(0.0f);
	for(size_t x = 0; x < mat_dim; x++)
	{
		for(size_t y = 0; y < mat_dim; y++)
		{
			const int i_p = x + offset_i;
			const int j_p = y + offset_j;
			if (i_p >= 0 && i_p < imageDim.y && j_p >= 0 && j_p < imageDim.x)
				output[pix] += image[i_p * imageDim.x + j_p] * gaussianMat[x * mat_dim + y];
		}
	}
}

__global__ void gaussianBlur(const uchar4* image, const int kernelSize, const float* gaussianMat, uchar4* output, 
							 const int2 imageDim)
{
	const size_t pix = blockDim.x * blockIdx.x + threadIdx.x;
	const int i = pix / imageDim.x;
	const int j = pix % imageDim.x;
	const int mat_dim = 2 * kernelSize + 1;
	const int offset_i = i - kernelSize;
	const int offset_j = j - kernelSize;
	float4 rgba = make_float4(0.0f);
	for(size_t x = 0; x < mat_dim; x++)
	{
		for(size_t y = 0; y < mat_dim; y++)
		{
			const int i_p = x + offset_i;
			const int j_p = y + offset_j;
			const int pos = i_p * imageDim.x + j_p;
			if (i_p >= 0 && i_p < imageDim.y && j_p >= 0 && j_p < imageDim.x)
			{
				rgba += make_float4(image[pos].x, image[pos].y, image[pos].z, image[pos].w) * gaussianMat[x * mat_dim + y];
			}
		}
	}
	output[pix] = make_uchar4(rgba.x, rgba.y, rgba.z, rgba.w);
}

cudaError_t ApplyGaussianBlur(RTbuffer image, const int2 dim, const int kernelSize, const float sigma_squared)
{
	const unsigned int blocks = dim.x * dim.y / threadsperblock;
	const unsigned int mat_dim = 2 * kernelSize + 1;
	const unsigned int mat_size = mat_dim * mat_dim;
	float* gaussianMatrix;
	cux(cudaMallocManaged(&gaussianMatrix, mat_size * sizeof(float), cudaMemAttachGlobal));

	// Create the Gaussian Weight Matrix
	float norm = 0.0f;
	for(unsigned int i = 0; i < mat_size; i++)
	{
		const int x = i % mat_dim - kernelSize;
		const int y = i / mat_dim - kernelSize;
		gaussianMatrix[i] = 0.5f * M_1_PIf / sigma_squared * powf(M_Ef, -0.5f * (x * x + y * y) / sigma_squared);
	}

	// Normalize the Gaussian Matrix
	for(size_t i = 0; i < mat_size; i++)
	{
		gaussianMatrix[i] /= norm;
	}

	// Launch the Gaussian Blur kernel
	void *data, *output;
	size_t bufferSize;
	rtBufferMap(image, &data);
	RTformat buffer_format;
	rtBufferGetFormat(image, &buffer_format);
	switch (buffer_format) {
		case RT_FORMAT_UNSIGNED_BYTE4:
			bufferSize = dim.x * dim.y * sizeof(uchar4);
			cux(cudaMalloc(&output, bufferSize));
			gaussianBlur<<<blocks, threadsperblock>>>((uchar4*)data, kernelSize, gaussianMatrix, (uchar4*)output, dim);
			break;
		case RT_FORMAT_FLOAT3:
			bufferSize = dim.x * dim.y * sizeof(float3);
			cux(cudaMalloc(&output, bufferSize));
			gaussianBlur<<<blocks, threadsperblock>>>((float3*)data, kernelSize, gaussianMatrix, (float3*)output, dim);
			break;
		default:
			return cudaErrorNotSupported;
	}	
	cux(cudaDeviceSynchronize());

	// Copy the blurred image to the original buffer and release the memory
	cux(cudaMemcpy(data, output, bufferSize, cudaMemcpyDefault));
	rtBufferUnmap(image);
	cux(cudaFree(output));
	cux(cudaFree(gaussianMatrix));
	return cudaSuccess;
}