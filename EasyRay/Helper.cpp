/*****************************************************************************
EasyRay version 2.0

Copyright � 2020 Ritwik Das

This file is part of EasyRay 2.0 - Ray-tracing renderer for 3D scenes/models.

EasyRay 2.0 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software in
some published article, an acknowledgment or a link to the software
webpage would be appreciated.

2. This notice may not be removed or altered from any source distribution.

EasyRay 2.0 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with EasyRay 2.0.  If not, see <http://www.gnu.org/licenses/>.

Author:
Ritwik Das - ritwik.sami1990@gmail.com

****************************************************************************/

#include "stdafx.h"
#include "Helper.hpp"
#include "lodepng.h"
#include <GL/glut.h>

// realloc() equivalent in device memory
template<typename T>
T* cudaRealloc(void* mem, size_t curSize, size_t newSize)
{
	void* newmem;
	if (newSize >= curSize && cudaMalloc(&newmem, newSize * sizeof(T)) == cudaSuccess)
	{
		cux(cudaMemcpy(newmem, mem, curSize * sizeof(T), cudaMemcpyDeviceToDevice));
		cux(cudaFree(mem));
		return reinterpret_cast<T*>(newmem);
	}

	return NULL;
}

RTresult SavePPM(const unsigned char *Pix, std::string fname, int wid, int hgt, int chan)
{
	if (Pix == NULL || chan < 1 || wid < 1 || hgt < 1) {
		fprintf(stderr, "Image is not defined. Not saving.\n");
		return RT_ERROR_UNKNOWN;
	}

	if (chan < 1 || chan > 4) {
		fprintf(stderr, "Can't save a X channel image as a PPM.\n");
		return RT_ERROR_UNKNOWN;
	}

	std::ofstream OutFile(fname, std::ios::out | std::ios::binary);
	if (!OutFile.is_open()) {
		fprintf(stderr, "Could not open file for SavePPM\n");
		return RT_ERROR_UNKNOWN;
	}

	bool is_float = false;
	OutFile << 'P';
	OutFile << ((chan == 1 ? (is_float ? 'Z' : '5') : (chan == 3 ? (is_float ? '7' : '6') : '8'))) << std::endl;
	OutFile << wid << " " << hgt << std::endl << 255 << std::endl;

	OutFile.write(reinterpret_cast<char*>(const_cast<unsigned char*>(Pix)), wid * hgt * chan * (is_float ? 4 : 1));

	OutFile.close();

	return RT_SUCCESS;
}

RTresult checkBuffer(RTbuffer buffer)
{
	// Check to see if the buffer is two dimensional
	unsigned int dimensionality;
	RTresult result;
	result = rtBufferGetDimensionality(buffer, &dimensionality);
	if (result != RT_SUCCESS) return result;
	if (2 != dimensionality) {
		fprintf(stderr, "Dimensionality of the buffer is %u instead of 2\n", dimensionality);
		return RT_ERROR_INVALID_VALUE;
	}
	// Check to see if the buffer is of type float{1,3,4} or uchar4
	RTformat format;
	result = rtBufferGetFormat(buffer, &format);
	if (result != RT_SUCCESS) return result;
	if (RT_FORMAT_FLOAT != format && RT_FORMAT_FLOAT4 != format && RT_FORMAT_FLOAT3 != format && RT_FORMAT_UNSIGNED_BYTE4 != format) {
		fprintf(stderr, "Buffer's format isn't float, float3, float4, or uchar4\n");
		return RT_ERROR_INVALID_VALUE;
	}

	return RT_SUCCESS;
}

RTresult saveImageFile(std::string filename, RTbuffer buffer)
{
	RTresult result;
	GLsizei width, height;
	RTsize buffer_width, buffer_height;

	result = checkBuffer(buffer);
	if (result != RT_SUCCESS) {
		fprintf(stderr, "Buffer isn't the right format. Didn't pass.\n");
		return result;
	}

	GLvoid* imageData;
	result = rtBufferMap(buffer, &imageData);
	if (result != RT_SUCCESS) {
		// Get error from context
		RTcontext context;
		const char* error;
		rtBufferGetContext(buffer, &context);
		rtContextGetErrorString(context, result, &error);
		fprintf(stderr, "Error mapping image buffer: %s\n", error);
		exit(2);
	}
	if (0 == imageData) {
		fprintf(stderr, "data in buffer is null.\n");
		exit(2);
	}
	result = rtBufferGetSize2D(buffer, &buffer_width, &buffer_height);
	if (result != RT_SUCCESS) {
		// Get error from context
		RTcontext context;
		const char* error;
		rtBufferGetContext(buffer, &context);
		rtContextGetErrorString(context, result, &error);
		fprintf(stderr, "Error getting _dimensions of buffer: %s\n", error);
		exit(2);
	}
	width = static_cast<GLsizei>(buffer_width);
	height = static_cast<GLsizei>(buffer_height);

	std::vector<unsigned char> pix(width * height * 3);
	RTformat buffer_format;

	result = rtBufferGetFormat(buffer, &buffer_format);

	switch (buffer_format) {
	case RT_FORMAT_UNSIGNED_BYTE4:
		// Data is BGRA and upside down, so we need to swizzle to RGB
		for (int j = height - 1; j >= 0; --j) {
			unsigned char *dst = &pix[0] + (3 * width*(height - 1 - j));
			unsigned char *src = ((unsigned char*)imageData) + (4 * width*j);
			for (int i = 0; i < width; i++) {
				*dst++ = *(src + 2);
				*dst++ = *(src + 1);
				*dst++ = *(src + 0);
				src += 4;
			}
		}
		break;

	case RT_FORMAT_FLOAT:
		// This buffer is upside down
		for (int j = height - 1; j >= 0; --j) {
			unsigned char *dst = &pix[0] + width*(height - 1 - j);
			float* src = ((float*)imageData) + (3 * width*j);
			for (int i = 0; i < width; i++) {
				int P = static_cast<int>((*src++) * 255.0f);
				unsigned int Clamped = P < 0 ? 0 : P > 0xff ? 0xff : P;

				// write the pixel to all 3 channels
				*dst++ = static_cast<unsigned char>(Clamped);
				*dst++ = static_cast<unsigned char>(Clamped);
				*dst++ = static_cast<unsigned char>(Clamped);
			}
		}
		break;

	case RT_FORMAT_FLOAT3:
		// This buffer is upside down
		for (int j = height - 1; j >= 0; --j) {
			unsigned char *dst = &pix[0] + (3 * width*(height - 1 - j));
			float* src = ((float*)imageData) + (3 * width*j);
			for (int i = 0; i < width; i++) {
				for (int elem = 0; elem < 3; ++elem) {
					int P = static_cast<int>((*src++) * 255.0f);
					unsigned int Clamped = P < 0 ? 0 : P > 0xff ? 0xff : P;
					*dst++ = static_cast<unsigned char>(Clamped);
				}
			}
		}
		break;

	case RT_FORMAT_FLOAT4:
		// This buffer is upside down
		for (int j = height - 1; j >= 0; --j) {
			unsigned char *dst = &pix[0] + (3 * width*(height - 1 - j));
			float* src = ((float*)imageData) + (4 * width*j);
			for (int i = 0; i < width; i++) {
				for (int elem = 0; elem < 3; ++elem) {
					int P = static_cast<int>((*src++) * 255.0f);
					unsigned int Clamped = P < 0 ? 0 : P > 0xff ? 0xff : P;
					*dst++ = static_cast<unsigned char>(Clamped);
				}

				// skip alpha
				src++;
			}
		}
		break;

	default:
		fprintf(stderr, "Unrecognized buffer data type or format.\n");
		exit(2);
		break;
	}

	if (filename.substr(filename.length() - 3, 3) == "ppm")
		SavePPM(&pix[0], filename, width, height, 3);
	else if (filename.substr(filename.length() - 3, 3) == "png")
		lodepng::encode(filename, pix, width, height, LCT_RGB, 8);

	// Now unmap the buffer
	result = rtBufferUnmap(buffer);
	if (result != RT_SUCCESS) {
		// Get error from context
		RTcontext context;
		const char* error;
		rtBufferGetContext(buffer, &context);
		rtContextGetErrorString(context, result, &error);
		fprintf(stderr, "Error unmapping image buffer: %s\n", error);
		exit(2);
	}

	return RT_SUCCESS;
}

// Load a 3D model file into s (file = .obj, .ply, etc.)
void erScene::LoadScene(std::string file)
{
	std::cout << "Loading file: " << file << "..." << std::endl;
	float4 defaultColor = make_float4(0.6f, 0.6f, 0.6f, 1.0f);

	//Assimp scene object
	const struct aiScene* scene = NULL;
	aiVector3D scene_min, scene_max, scene_center;
	scene = aiImportFile(file.c_str(), aiProcessPreset_TargetRealtime_MaxQuality);

	// Empty scene
	if (!scene)	return;

	// Import geom data from Assimp
	if (scene->HasMeshes())
	{
		for (size_t m = 0; m < scene->mNumMeshes; m++)
		{
			// Extract mesh material
			aiMesh* mesh = scene->mMeshes[m];
			erMaterial mat;
			aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
			aiColor4D color;

			// Diffuse Color
			if (material->Get(AI_MATKEY_COLOR_DIFFUSE, color) == AI_SUCCESS)
			{
				mat._diffColor = make_float4(color.r, color.g, color.b, color.a);
			}

			// Specular Color
			if (material->Get(AI_MATKEY_COLOR_SPECULAR, color) == AI_SUCCESS)
			{
				mat._specColor = make_float4(color.r, color.g, color.b, color.a);
			}

			// Ambient Color
			if (material->Get(AI_MATKEY_COLOR_AMBIENT, color) == AI_SUCCESS)
			{
				mat._ambtColor = make_float4(color.r, color.g, color.b, color.a);
			}

			// Emissive Color
			if (material->Get(AI_MATKEY_COLOR_EMISSIVE, color) == AI_SUCCESS)
			{
				mat._emsvColor = make_float4(color.r, color.g, color.b, color.a);
			}

			// Shininess
			float shininess, strength;
			if (material->Get(AI_MATKEY_SHININESS, shininess) == AI_SUCCESS)
			{
				mat._shininess = shininess;
				mat._specStrength = 1.0f;
				if (material->Get(AI_MATKEY_SHININESS_STRENGTH, strength) == AI_SUCCESS)
					mat._specStrength = strength;
			}

			// Refractive index
			float refr;
			if (material->Get(AI_MATKEY_REFRACTI, refr) == AI_SUCCESS)
			{
				mat._refractionIndex = refr;
			}

			// Opacity Color
			float d;
			if (material->Get(AI_MATKEY_OPACITY, d) == AI_SUCCESS)
			{
				mat._opacity = d;
			}

			// Textures
			int texIndex = 0;
			aiString path;

			if (material->GetTexture(aiTextureType_DIFFUSE, texIndex, &path) == AI_SUCCESS)
			{
				std::string textureName = path.data;
				std::string fullPath = textureName;
				if (textureName.rfind('/') == std::string::npos && textureName.rfind('\\') == std::string::npos)
				{
					std::string dir;
					size_t lastSlash = file.rfind('/');
					if (lastSlash != std::string::npos)
						dir = file.substr(0, lastSlash + 1);
					fullPath = dir + textureName;
				}

				// Load texture image
				std::vector<unsigned char> imageVec;
				if (0 == lodepng::decode(imageVec, mat._texWidth, mat._texHeight, fullPath, LCT_RGBA, 8))
				{
					const size_t imageSize = mat._texWidth * mat._texHeight * sizeof(uchar4);
					cux(cudaMalloc(&mat._texture, imageSize));
					cux(cudaMemcpy(mat._texture, imageVec.data(), imageSize, cudaMemcpyHostToDevice));
				}
			}

			std::vector<erVertex> mVerts;
			std::vector<float3> mNorms;
			std::vector<erTriangle> mTriags;
			size_t nVerts = 0;
			size_t nTriags = 0;
			erVertex box[2];

			if (mesh->HasFaces())
			{
				nVerts = mesh->mNumVertices;
				for (size_t f = 0; f < mesh->mNumFaces; f++)
				{
					aiFace face = mesh->mFaces[f];
					nTriags += face.mNumIndices - 2;
				}

				mVerts.reserve(nVerts);
				if (mesh->mNormals)
					mNorms.reserve(nVerts);
				mTriags.reserve(nTriags);

				// Add faces
				for (size_t f = 0; f < mesh->mNumFaces; f++)
				{
					aiFace face = mesh->mFaces[f];
					int nInd = face.mNumIndices;
					for (int idx = 2; idx < nInd; idx++)
					{
						mTriags.push_back(make_int3(face.mIndices[0], face.mIndices[idx - 1], face.mIndices[idx]));
					}
				}

				int nColors = mesh->GetNumColorChannels();
				for (size_t v = 0; v < nVerts; v++)
				{
					// Add vertices
					const erVertex vert = make_float4(mesh->mVertices[v].x, mesh->mVertices[v].y, mesh->mVertices[v].z, 1);
					mVerts.push_back(vert);
					box[0].x = optix::fminf(box[0].x, vert.x);
					box[0].y = optix::fminf(box[0].y, vert.y);
					box[0].z = optix::fminf(box[0].z, vert.z);
					box[1].x = optix::fmaxf(box[1].x, vert.x);
					box[1].y = optix::fmaxf(box[1].y, vert.y);
					box[1].z = optix::fmaxf(box[1].z, vert.z);

					// Add normals
					if (mesh->mNormals)
						mNorms.push_back(make_float3(mesh->mNormals[v].x, mesh->mNormals[v].y, mesh->mNormals[v].z));

					// Add texture coordinate
					if (mesh->HasTextureCoords(0))
					{
						aiVector3D uvw = mesh->mTextureCoords[0][v];
						_uv.push_back(make_float3(uvw.x, uvw.y, uvw.z));
					}
				}
			}

			// Add mesh to scene
			addInstance(
				erInstance(
					erGeometry(
						std::move(mVerts),
						std::move(mTriags),
						std::move(mNorms),
						box),
					mat.GetManagedCopy(),
					optix::Matrix4x4::identity()));
		}
	}

	// Release Assimp scene
	aiReleaseImport(scene);
}

#pragma region erMaterial
erMaterial::~erMaterial()
{
	if (_texture != NULL)
		cux(cudaFree(_texture));
}

erMaterial* erMaterial::GetManagedCopy()
{
	erMaterial* managedMat;
	cux(cudaMallocManaged(&managedMat, sizeof(erMaterial), cudaMemAttachGlobal));
	cux(cudaMemcpy(managedMat, this, sizeof(erMaterial), cudaMemcpyDefault));
	return managedMat;
}
#pragma endregion

#pragma region erGeometry
erGeometry::erGeometry(std::vector<erVertex>&& verts, std::vector<erTriangle>&& triangs, std::vector<float3>&& norms, erVertex bbox[2]) :
	_geomVertices(verts),
	_geomTriangles(triangs),
	_geomNormals(norms)
{
	_BBox[0] = bbox[0];
	_BBox[1] = bbox[1];
}

erGeometry::erGeometry(std::string file)
{
	if (file.length() == 0)
		return;

	AutoTimer _("Loading geometry from file (" + file + ")...");
	std::ifstream infile(file, std::ifstream::in);

	char ch;
	int zero;
	int nVertices, nTriangles;
	_BBox[0] = make_float4(FLT_MAX, FLT_MAX, FLT_MAX, 1);
	_BBox[1] = make_float4(-FLT_MAX, -FLT_MAX, -FLT_MAX, 1);
	infile >> ch >> ch >> ch;
	infile >> nVertices >> nTriangles >> zero;

	// Create the vertex and triangle buffers
	_geomVertices.reserve(nVertices);
	for (int i = 0; i < nVertices; i++)
	{
		float x, y, z;
		infile >> x >> y >> z;
		_geomVertices.push_back(make_float4(x, y, z, 1.0));

		//Compute bounding box
		_BBox[0].x = optix::fminf(_BBox[0].x, x);
		_BBox[0].y = optix::fminf(_BBox[0].y, y);
		_BBox[0].z = optix::fminf(_BBox[0].z, z);
		_BBox[1].x = optix::fmaxf(_BBox[1].x, x);
		_BBox[1].y = optix::fmaxf(_BBox[1].y, y);
		_BBox[1].z = optix::fmaxf(_BBox[1].z, z);
	}

	_geomTriangles.reserve(nTriangles);
	for (int i = 0; i < nTriangles; i++)
	{
		int n, v0, v1, v2;
		infile >> n >> v0 >> v1 >> v2;
		_geomTriangles.push_back(make_int3(v0, v1, v2));
	}
	infile.close();
}
#pragma endregion

#pragma region erInstance
erInstance::erInstance(erGeometry&& geom, const erMaterial* mat, const optix::Matrix4x4 transform):
	_geom(geom),
	_mat(mat)
{
	AutoTimer _("Creating geometry instance (verts="
		+ std::to_string(_geom._geomVertices.size())
		+ ", normals=" + std::to_string(_geom._geomNormals.size())
		+ ", triangles=" + std::to_string(_geom._geomTriangles.size()) + ")...");

	std::transform(
		_geom._geomVertices.begin(),
		_geom._geomVertices.end(),
		_geom._geomVertices.begin(),
		[=](erVertex v) { return transform * v; });
	std::transform(
		_geom._geomNormals.begin(),
		_geom._geomNormals.end(),
		_geom._geomNormals.begin(),
		[=](float3 n) { return make_float3(transform * make_float4(n, 1.0f)); });

	// Compute bounding box
	_BBox[0] = make_float4(FLT_MAX, FLT_MAX, FLT_MAX, 1);
	_BBox[1] = make_float4(-FLT_MAX, -FLT_MAX, -FLT_MAX, 1);
	for (size_t v = 0; v < _geom._geomVertices.size(); v++)
	{
		_BBox[0].x = optix::fminf(_BBox[0].x, _geom._geomVertices[v].x);
		_BBox[0].y = optix::fminf(_BBox[0].y, _geom._geomVertices[v].y);
		_BBox[0].z = optix::fminf(_BBox[0].z, _geom._geomVertices[v].z);
		_BBox[1].x = optix::fmaxf(_BBox[1].x, _geom._geomVertices[v].x);
		_BBox[1].y = optix::fmaxf(_BBox[1].y, _geom._geomVertices[v].y);
		_BBox[1].z = optix::fmaxf(_BBox[1].z, _geom._geomVertices[v].z);
	}
}

erInstance::erInstance(erInstance&& instance):
	_geom(std::move(instance._geom)),
	_mat(instance._mat)
{
	_BBox[0] = instance._BBox[0];
	_BBox[1] = instance._BBox[1];
	instance._mat = NULL;
}

erInstance::~erInstance()
{
	if (_mat)
		cux(cudaFree((void*)_mat));
}
#pragma endregion

#pragma region erScene
erScene::~erScene()
{
	if (_vertices) cux(cudaFree(_vertices));
	if (_normals__D) cux(cudaFree(_normals__D));
	if (_indices) cux(cudaFree(_indices));
	if (_matID__D) cux(cudaFree(_matID__D));
	if (_uv__D) cux(cudaFree(_uv__D));
	if (_DEVgeomInstances) cux(cudaFree(_DEVgeomInstances));
}

void erScene::addInstance(erInstance&& inst)
{
	AutoTimer _("Adding geometry instance (verts="
		+ std::to_string(inst._geom._geomVertices.size())
		+ ", normals=" + std::to_string(inst._geom._geomNormals.size())
		+ ", triangles=" + std::to_string(inst._geom._geomTriangles.size()) + ")...");

	// Reallocate arrays
	auto&& vertices = inst._geom._geomVertices;
	auto&& normals = inst._geom._geomNormals;
	auto&& triangles = inst._geom._geomTriangles;

	_vertices = cudaReallocManaged<erVertex>(_vertices, _nVertices, vertices.size() + _nVertices);
	if (normals.size() > 0)
		_normals__D = cudaRealloc<float3>(_normals__D, _nVertices, normals.size() + _nVertices);
	_indices = cudaReallocManaged<erTriangle>(_indices, _nTriangles, triangles.size() + _nTriangles);

	// Copy new vertices and normals
	cux(cudaMemcpy(_vertices + _nVertices, vertices.data(), vertices.size() * sizeof(erVertex), cudaMemcpyHostToDevice));
	cux(cudaMemcpy(_normals__D + _nVertices, normals.data(), normals.size() * sizeof(float3), cudaMemcpyHostToDevice));

	// Copy new indices
	for (unsigned int ind = 0; ind < triangles.size(); ind++)
	{
		_indices[ind + _nTriangles] = make_int3(
			static_cast<int>(triangles[ind].x + _nVertices),
			static_cast<int>(triangles[ind].y + _nVertices),
			static_cast<int>(triangles[ind].z + _nVertices));
		_matID.push_back(_geomInstances.size());
	}

	_nVertices += vertices.size();
	_nTriangles += triangles.size();
	
	_geomInstances.push_back(std::move(inst));
}

void erScene::calculateBBox()
{
	AutoTimer _("Calculating BB...");

	_BBox[0] = make_float4(FLT_MAX, FLT_MAX, FLT_MAX, 1);
	_BBox[1] = make_float4(-FLT_MAX, -FLT_MAX, -FLT_MAX, 1);

	for (unsigned int inst = 0; inst < _geomInstances.size(); inst++)
	{
		_BBox[0].x = optix::fminf(_BBox[0].x, _geomInstances[inst]._BBox[0].x);
		_BBox[0].y = optix::fminf(_BBox[0].y, _geomInstances[inst]._BBox[0].y);
		_BBox[0].z = optix::fminf(_BBox[0].z, _geomInstances[inst]._BBox[0].z);
		_BBox[1].x = optix::fmaxf(_BBox[1].x, _geomInstances[inst]._BBox[1].x);
		_BBox[1].y = optix::fmaxf(_BBox[1].y, _geomInstances[inst]._BBox[1].y);
		_BBox[1].z = optix::fmaxf(_BBox[1].z, _geomInstances[inst]._BBox[1].z);
	}
}

void erScene::commit(const bool transform)
{
	AutoTimer _("Committing scene (building acceleration structures)...");

	// Copy material indices on device
	if (_matID__D)
		cux(cudaFree(_matID__D));
	cux(cudaMalloc(&_matID__D, _matID.size() * sizeof(size_t)));
	cux(cudaMemcpy(_matID__D, _matID.data(), _matID.size() * sizeof(size_t), cudaMemcpyHostToDevice));

	// Copy texture coordinates
	const size_t uvSize = _uv.size() * sizeof(float3);
	if (uvSize > 0)
	{
		if (_uv__D)
			cux(cudaFree(_uv__D));
		cux(cudaMalloc(&_uv__D, uvSize));
		cux(cudaMemcpy(_uv__D, _uv.data(), uvSize, cudaMemcpyDefault));
	}

	// Copy geomInstances
	if (_DEVgeomInstances)
		cux(cudaFree(_DEVgeomInstances));
	cux(cudaMalloc(&_DEVgeomInstances, _geomInstances.size() * sizeof(erInstance)));
	cux(cudaMemcpy(_DEVgeomInstances, _geomInstances.data(), _geomInstances.size() * sizeof(erInstance), cudaMemcpyHostToDevice));

	if (transform)
	{
		// Transform vertices
		calculateBBox();
		const float4 range = _BBox[1] - _BBox[0];
		const float4 center = (_BBox[0] + _BBox[1]) / 2;
		const float4 scale = CUBE_SIZE / range;
		const float s = optix::fminf(scale.x, optix::fminf(scale.y, scale.z)) - 4;
		thrust::transform(_vertices, _vertices + _nVertices, _vertices, erTransGeom(s, center, optix::Matrix4x4::identity()));

		// Transform bounding boxes
		thrust::transform(_BBox, _BBox + 2, _BBox, erTransGeom(s, center, optix::Matrix4x4::identity()));
		for (unsigned int inst = 0; inst < _geomInstances.size(); inst++)
		{
			thrust::transform(_geomInstances[inst]._BBox, _geomInstances[inst]._BBox + 2, _geomInstances[inst]._BBox, erTransGeom(s, center, optix::Matrix4x4::identity()));
		}
	}
}
#pragma endregion